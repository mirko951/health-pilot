<?php

namespace App\Http\Controllers;

use App\Models\Recipecategories;
use Illuminate\Http\Request;

class RecipesCategoriesController extends Controller
{
    public function index()
    {
        $recipes = new Recipecategories();
        $recipes_list = $recipes->all();
        return view('auth.recipes_list')->with('recipes_list', $recipes_list);
    }
}
