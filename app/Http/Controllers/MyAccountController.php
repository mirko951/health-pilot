<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\UserExercises;
use App\Models\UserMeals;
use App\Models\Recipes;
use App\Models\Exercises;

class MyAccountController extends Controller
{
    public function myprogress(Request $request)
    {
        $selectedMonth = $request->input('selected_month');
        $startDate = Carbon::parse($selectedMonth)->startOfMonth();
        $endDate = Carbon::parse($selectedMonth)->endOfMonth();

        $user = auth()->user();
        $meals = UserMeals::where('user_id', $user->id)
            ->whereBetween('day', [$startDate, $endDate])
            ->get();

        $exercises = UserExercises::where('user_id', $user->id)
            ->whereBetween('day', [$startDate, $endDate])
            ->get();

        $meals_kcal_total = $this->calculateMealsKcalTotal($meals);
        $exercises_kcal_total = $this->calculateExercisesKcalTotal($exercises);

        return view('auth.myprogress', compact('meals_kcal_total', 'exercises_kcal_total'));
    }

    private function calculateMealsKcalTotal($meals)
    {
        $meals_kcal_total = [];

        foreach ($meals as $meal) {
            $recipe = Recipes::find($meal->recipe_id);

            if ($recipe) {
                $cal = $recipe->calories_per_serving;
                $timestamp = strtotime($meal->day);
                $day_number = date('j', $timestamp);

                if (!isset($meals_kcal_total[$day_number])) {
                    $meals_kcal_total[$day_number] = 0;
                }

                $meals_kcal_total[$day_number] += $cal;
            }
        }

        return $meals_kcal_total;
    }

    private function calculateExercisesKcalTotal($exercises)
    {
        $exercises_kcal_total = [];

        foreach ($exercises as $exercise) {
            $exerciseDetails = Exercises::find($exercise->exercise_id);

            if ($exerciseDetails) {
                $cal = $exerciseDetails->calories;
                $timestamp = strtotime($exercise->day);
                $day_number = date('j', $timestamp);

                if (!isset($exercises_kcal_total[$day_number])) {
                    $exercises_kcal_total[$day_number] = 0;
                }

                $exercises_kcal_total[$day_number] += $cal;
            }
        }

        return $exercises_kcal_total;
    }

    public function myinformation()
    {
        $user = auth()->user();
        $userProfile = User::where('id', $user->id)->first();
        
        $name = $userProfile->name ?? '';
        $age = $userProfile->age ?? '';
        $gender = $userProfile->gender ?? '';
    
        return view('auth.myinformation', compact('name', 'age', 'gender'));
    }
    
    public function account()
    {
        return view('auth.account');
    }
}
