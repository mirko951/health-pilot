<?php

namespace App\Http\Controllers;
use App\Models\Exercises;
use App\Models\Recipes;
use Illuminate\Http\Request;

class AboutUsController extends Controller
{
    public function index()
{
    $exercises = Exercises::take(2)->get();
    $recipes = Recipes::take(2)->get();
    return view('auth.about_us', compact('exercises', 'recipes'));
}

}
