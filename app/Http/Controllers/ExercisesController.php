<?php

namespace App\Http\Controllers;

use App\Models\exercises;
use App\Models\ExerciseCategories;
use Illuminate\Http\Request;

class ExercisesController extends Controller
{
    public function index()
    {
            $exercises = new exercises();
            $exercise = $exercises->all();
            return view('auth.exercises')->with('exercises', $exercise);
       
    }

    public function exercises($shortname)	
    {
        $exercises = new exercises();
        $exercise_category = ExerciseCategories::where('shortname', '=', $shortname)->first(); 
        $exercises = exercises::where('category_id', '=', $exercise_category->category_id)->get();
        return view('auth.exercises')->with('exercises', $exercises)->with('exercise_category', $exercise_category);
    }
}
