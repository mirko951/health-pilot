<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Exercises;
use Illuminate\Support\Facades\Auth;

class AdminExercisesController extends Controller
{
    public function createExerciseForm() {
        if (Auth::user()->admin == 0) {
            return redirect()->route('dashboard');
        }

        return view('admin.create_exercise');
    }

    // Store a new exercise
    public function storeExercise(Request $request) {
        if (Auth::user()->admin == 0) {
            return redirect()->route('dashboard')->with('error', 'Permission denied');
        }
    
        // Validate the form data
        $validatedData = $request->validate([
            'name' => 'required|string|max:100',
            'description' => 'nullable|string',
            'category_id' => 'required|integer',
            'image_url' => 'nullable',
            'rep' => 'required|string|max:128',
            'calories' => 'required|integer|min:0|max:999',
        ]);
    
        // Create and save the exercise
        $exercise = new Exercises();
        $exercise->name = $validatedData['name'];
        $exercise->description = $validatedData['description'];
        $exercise->category_id = $validatedData['category_id'];
         // Check if 'image_url' exists in the request before accessing it
         if ($request->hasFile('image_url')) {
            $file = $request->file('image_url');
    
            // Generate a unique filename
            $filename = uniqid() . '.' . $file->getClientOriginalExtension();
    
            // Store the file in the public directory under 'public/resources/images/coaches'
            $file->storeAs('/exercises', $filename, 'public_uploads');
    
            // Set the 'photo' column in the exercise model to the file name
            $exercise->image_url = 'resources/images/exercises/' . $filename;
        
    }
        $exercise->rep = $validatedData['rep'];
        $exercise->calories = $validatedData['calories'];
        // Save the exercise to the database
        try {
            $exercise->save();
            // dd($exercise);
            return redirect()->route('admin.exercises')->with('success', 'Exercise created successfully');
        } catch (\Exception $e) {
            return redirect()->route('admin.exercises')->with('error', 'Failed to create exercise');
        }
    }

    // Remove an exercise
    public function deleteExercise($exercise_id) {
        if (Auth::user()->admin == 0) {
            return redirect()->route('dashboard');
        }
    
        $exercise = Exercises::find($exercise_id);
    
        if (!$exercise) {
            return redirect()->route('admin.exercises')->with('error', 'Exercise not found');
        }
    
        $exercise->delete(); // Soft delete
    
        return redirect()->route('admin.exercises')->with('success', 'Exercise marked as deleted');
    }

    // Display the list of exercises
    public function viewExercises() {
        if (Auth::user()->admin == 0) {
            return redirect()->route('dashboard');
        }

        $exercises = Exercises::withTrashed()->get();
    
        return view('admin.exercises', compact('exercises'));
    }
    
    

    public function restoreAllExercises() {
        $softDeletedExercises = Exercises::onlyTrashed()->get();
    
        foreach ($softDeletedExercises as $exercise) {
            $exercise->restore();
        }
    
        return redirect()->route('admin.exercises')->with('success', 'All exercises restored successfully');
    }
    

    public function editExerciseForm($exercise_id) {
        
        $exercise = Exercises::find($exercise_id);

        if (!$exercise) {
            return redirect()->route('admin.exercises')->with('error', 'Exercise not found');
        }
    
        return view('admin.edit_exercise', compact('exercise'));
    }
    
    public function updateExercise(Request $request, $exercise_id) {

        $validatedData = $request->validate([
            'name' => 'required|string|max:255',
            'image_url' => 'required|string|max:200',
            'description' => 'required|string',
            'rep' => 'required|string|max:128',
            'calories' => 'required|integer|min:0', 
        ]);
    
        $exercise = Exercises::find($exercise_id);

        if (!$exercise) {
            return redirect()->route('admin.exercises')->with('error', 'Exercise not found');
        }

        $exercise->name = $validatedData['name'];
        $exercise->image_url = $validatedData['image_url'];
        $exercise->description = $validatedData['description'];
        $exercise->rep = $validatedData['rep'];
        $exercise->calories = $validatedData['calories'];
    
        try {
            $exercise->save();
            return redirect()->route('admin.exercises')->with('success', 'Exercise updated successfully');
        } catch (\Exception $e) {
            return redirect()->route('admin.exercises')->with('error', 'Failed to update exercise');
        }
    }

}
