<?php

namespace App\Http\Controllers;

use App\Models\usermeals;
use Illuminate\Http\Request;

class UserMealsController extends Controller
{
    public function store(Request $request)
    {
        if(isset($request->selected_recipes)){
        foreach ($request->selected_recipes as $recipe_id) {
            UserMeals::create([
                'recipe_id' => $recipe_id,
                'user_id' => auth()->user()->id 
            ]);
        }}
        return redirect()->route('myprogress');
    }
}
