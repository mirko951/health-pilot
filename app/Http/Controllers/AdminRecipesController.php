<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\Recipes;
class AdminRecipesController extends Controller
{
      public function createrecipeForm() 
      {
        if (Auth::user()->admin == 0) {
            return redirect()->route('dashboard');
        }

        return view('admin.create_recipe');
    }

    // Store a new recipe
    public function storeRecipe(Request $request) 
    {
    // Validate the incoming data
    $validatedData = $request->validate([
        'name' => 'required|max:100',
        'category_id' => 'required|integer',
        'image_url' => 'nullable',
        'ingredients' => 'required',
        'method' => 'required',
        'calories_per_serving' => 'required|integer|min:0|max:999',
    ]);

    // Create a new recipe instance
    $recipe = new Recipes;
    $recipe->name = $validatedData['name'];
    $recipe->category_id = $validatedData['category_id'];

 // Check if 'image_url' exists in the request before accessing it
 if ($request->hasFile('image_url')) 
    {
        $file = $request->file('image_url');

        // Generate a unique filename
        $filename = uniqid() . '.' . $file->getClientOriginalExtension();

        // Store the file in the public directory under 'public/resources/images/coaches'
        $file->storeAs('/recipes', $filename, 'public_uploads');

        // Set the 'photo' column in the exercise model to the file name
        $recipe->image_url = 'resources/images/recipes/' . $filename;
    }
    $recipe->ingredients = $validatedData['ingredients'];
    $recipe->method = $validatedData['method'];
    $recipe->calories_per_serving = $validatedData['calories_per_serving'];

// Save the recipe to the database
try {
    $recipe->save();
    return redirect()->route('admin.recipes')->with('success', 'Recipe created successfully');
} catch (\Exception $e) {
    return redirect()->route('admin.recipes')->with('error', 'Failed to create recipe');
}
}
    
    

    // Remove an recipe
    public function deleterecipe($recipe_id) {
        if (Auth::user()->admin == 0) {
            return redirect()->route('dashboard');
        }
    
        $recipe = Recipes::find($recipe_id);
    
        if (!$recipe) {
            return redirect()->route('admin.recipes')->with('error', 'recipe not found');
        }
    
        $recipe->delete(); // Soft delete
    
        // Redirect with a success message
        return redirect()->route('admin.recipes')->with('success', 'recipe marked as deleted');
    }

    // Display the list of Recipes
    public function viewRecipes() {
        if (Auth::user()->admin == 0) {
            return redirect()->route('dashboard');
        }
    
        // Retrieve both non-deleted and soft-deleted Recipes
        $recipes = Recipes::withTrashed()->get();
    
        return view('admin.recipes', compact('recipes'));
    }
    
    
    

    public function restoreAllRecipes()
    {
        if (Auth::user()->admin == 0) {
            return redirect()->route('dashboard');
        }
        // Retrieve all soft-deleted Recipes
        $softDeletedRecipes = Recipes::onlyTrashed()->get();
    
        // Restore each soft-deleted recipe
        foreach ($softDeletedRecipes as $recipe) {
            $recipe->restore();
        }
    
        return redirect()->route('admin.recipes')->with('success', 'All Recipes restored successfully');
    }
    

    public function editrecipeForm($recipe_id) {
        // Retrieve the recipe by ID
        $recipe = Recipes::find($recipe_id);
    
        // Check if the recipe exists
        if (!$recipe) {
            return redirect()->route('admin.recipes')->with('error', 'recipe not found');
        }
    
        return view('admin.edit_recipe', compact('recipe'));
    }
    
    public function updaterecipe(Request $request, $recipe_id) {
        // die("saving");
        // Validate the form data
        $validatedData = $request->validate([
            'name' => 'required|string|max:255',
            // 'image_url' => 'required|string|max:200',
            'ingredients' => 'required|string',
            'method' => 'required|string',
            'calories_per_serving' => 'required|integer|min:0', 
        ]);
        // die('arrives here');
    
        // Retrieve the recipe by ID
        $recipe = Recipes::find($recipe_id);
        // die(json_encode($recipe));
        // Check if the recipe exists
        if (!$recipe) {
            return redirect()->route('admin.recipes')->with('error', 'recipe not found');
        }
    
        // Update recipe details
        $recipe->name = $validatedData['name'];
        // $recipe->image_url = $validatedData['image_url'];
        $recipe->ingredients = $validatedData['ingredients'];
        $recipe->method = $validatedData['method'];
        $recipe->calories_per_serving = $validatedData['calories_per_serving'];
    
        try {
            $recipe->save();
            return redirect()->route('admin.recipes')->with('success', 'Recipe updated successfully');
        } catch (\Exception $e) {
            // Handle the database exception, e.g., log the error or return an error message
            return redirect()->route('admin.recipes')->with('error', 'Failed to update recipe');
        }
    }


}
