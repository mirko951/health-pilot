<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Product;
use Illuminate\Support\Facades\Auth;


class AdminProductsController extends Controller
{
        public function createProductForm()
        {
            if (Auth::user()->admin == 0) {
                return redirect()->route('dashboard')->with('error', 'Permission denied');
            }
    
            return view('admin.create_product');
        }
    
        // Store a new product
        public function storeProduct(Request $request)
        {
            if (Auth::user()->admin == 0) {
                return redirect()->route('dashboard');
            }

            $validatedData = $request->validate([
            'product_name' => 'required|string|max:255',
            'photo' => 'required',
            'product_description' => 'required|string',
            'price' => 'required|string',
        ]);

        $product = new Product([
            'product_name' => $validatedData['product_name'],
            'product_description' => $validatedData['product_description'],
            'price' => $validatedData['price'],
        ]);

        // Check if 'image_url' exists in the request before accessing it
        if ($request->hasFile('photo')) 
        {
            $file = $request->file('photo');

            // Generate a unique filename
            $filename = uniqid() . '.' . $file->getClientOriginalExtension();

            // Store the file in the public directory under 'public/resources/images/coaches'
            $file->storeAs('/products', $filename, 'public_uploads');

            // Set the 'photo' column in the exercise model to the file name
            $product->photo = 'resources/images/products/' . $filename;
        }

        // Save the recipe to the database
        try {
            $product->save();
            return redirect()->route('admin.products')->with('success', 'Product created successfully');
        } catch (\Exception $e) {
            return redirect()->route('admin.products')->with('error', 'Failed to create product');
        }
        }
    
        // Remove a product
        public function deleteProduct($id)
        {
            if (Auth::user()->admin == 0) {
                return redirect()->route('dashboard')->with('error', 'Permission denied');
            }
    
            $product = Product::find($id);
    
            if (!$product) {
                return redirect()->route('admin.products')->with('error', 'Product not found');
            }
    
            $product->delete(); // Soft delete
    
            return redirect()->route('admin.products')->with('success', 'Product marked as deleted');
        }
    
        // Display the list of products
        public function viewProducts()
        {
            if (Auth::user()->admin == 0) {
                return redirect()->route('dashboard')->with('error', 'Permission denied');
            }
    
            // Retrieve both non-deleted and soft-deleted products
            $products = Product::withTrashed()->get();
    
            return view('admin.products', compact('products'));
        }
    
        // Restore all soft-deleted products
        public function restoreAllProducts()
        {
            if (Auth::user()->admin == 0) {
                return redirect()->route('dashboard')->with('error', 'Permission denied');
            }
    
            // Retrieve all soft-deleted products
            $softDeletedProducts = Product::onlyTrashed()->get();
    
            // Restore each soft-deleted product
            foreach ($softDeletedProducts as $product) {
                $product->restore();
            }
    
            return redirect()->route('admin.products')->with('success', 'All products restored successfully');
        }
    
        // Display the form to edit a product
        public function editProductForm($id)
        {
            if (Auth::user()->admin == 0) {
                return redirect()->route('dashboard');
            }
    
            $product = Product::find($id);
    
            if (!$product) {
                return redirect()->route('admin.products')->with('error', 'Product not found');
            }
    
            return view('admin.edit_product', compact('product'));
        }
    
        // Update a product
        public function updateProduct(Request $request, $id)
        {
            if (Auth::user()->admin == 0) {
                return redirect()->route('dashboard')->with('error', 'Permission denied');
            }
    
            // Validate the form data
            $validatedData = $request->validate([
                'product_name' => 'required|string|max:255',
                'photo' => 'nullable|image|',
                'product_description' => 'nullable|string',
                'price' => 'required|string|max:255',
            ]);

            $product = Product::find($id);

            if (!$product) {
                return redirect()->route('admin.products')->with('error', 'Product not found');
            }
        
            $product->product_name = $validatedData['product_name'];
            $product->product_description = $validatedData['product_description'];
            $product->price = $validatedData['price'];
        
            // Handle the photo update if a new photo is provided
            // if ($request->hasFile('photo')) {
            //     $photoPath = $request->file('photo')->store('products', 'public');
            //     $product->photo = 'storage/' . $photoPath;
            // }
        
            try {
                $product->save();
                return redirect()->route('admin.products')->with('success', 'Product updated successfully');
            } catch (\Exception $e) {
                return redirect()->route('admin.products')->with('error', 'Failed to update product');
            }
        }


 
}