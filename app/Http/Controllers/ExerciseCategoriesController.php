<?php

namespace App\Http\Controllers;

use App\Models\exercisecategories;
class ExerciseCategoriesController extends Controller
{
    public function index()
    {
        $exercises = new exercisecategories();
        $exercises_list = $exercises->all();
        // echo json_encode($excercises_list);
        return view('auth.exercise_list')->with('exercises_list', $exercises_list);
    }

}
