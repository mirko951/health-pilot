<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\CoachReview;


class CoachReviewController extends Controller
{
    public function create($coach_id)
    {
        // Load the view for leaving a review
        return view('auth.create')->with('coach_id', $coach_id);
    }

    public function store(Request $request)
    {
        // Validate the form data
        $request->validate([
            'rating' => 'required|integer|min:1|max:5',
            'comment' => 'required',
        ]);

        // Create a new review
        $review = new CoachReview([
            'user_id' => auth()->user()->id,
            'coach_id' => $request->input('coach_id'),
            'rating' => $request->input('rating'),
            'comment' => $request->input('comment'),
        ]);

        $review->save();

        return redirect()->back()->with('success', 'Review submitted successfully.');
    }
}
