<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Coach;
use Illuminate\Support\Facades\DB;use Illuminate\Support\Facades\Auth;
class AdminCoachesController extends Controller
{
    public function viewCoaches()
    {
        $coaches = Coach::all();
        
        $SoftDeletedCoaches = DB::table('Coaches')->whereNotNull('deleted_at')->get();
        return view('admin.coaches', compact('coaches', 'SoftDeletedCoaches'));
    }

    public function createCoachesForm()
    {
        return view('admin.create_coach');
    }

public function storeCoaches(Request $request)
{
    if (Auth::user()->admin == 0) {
        return redirect()->route('dashboard')->with('error', 'Permission denied');
    }

    $validatedData = $request->validate([
        'name' => 'required|string|max:100',
        'photo' => 'nullable|image|mimes:jpeg,png,jpg,gif',
        'description' => 'required|string|max:128',
        'email_address' => 'required|string|max:128',
    ]);

    $coach = new Coach();
    $coach->name = $validatedData['name'];
    $coach->email_address = $validatedData['email_address'];
    $coach->description = $validatedData['description'];

    // Check if 'photo' key exists in the validated data
    if ($request->hasFile('photo')) {
        $file = $request->file('photo');

        // Generate a unique filename
        $filename = uniqid() . '.' . $file->getClientOriginalExtension();

        // Store the file in the public directory under 'public/resources/images/coaches'
        $file->storeAs('/coaches', $filename, 'public_uploads');

        // Set the 'photo' column in the coach model to the file name
        $coach->photo = 'resources/images/coaches/' . $filename;
    }

    $coach->save();

    return redirect()->route('admin.coaches')->with('success', 'Coach created successfully');
}


    public function editCoachesForm($id)
    {
        $coach = Coach::find($id);

        if (!$coach) {
            return redirect()->route('admin.coaches')->with('error', 'Coach not found');
        }

        return view('admin.edit_coach', compact('coach'));
    }
    
    

    public function updateCoaches(Request $request, $id)
    {
        $validatedData = $request->validate([
            'name' => 'required|string|max:100',
            'photo' => 'nullable|string|max:128',
            'description' => 'required|string|max:128',
            'email_address' => 'required|string|max:128',
        ]);

        $coach = Coach::find($id);

        if (!$coach) {
            return redirect()->route('admin.coaches')->with('error', 'Coach not found');
        }

        $coach->name = $validatedData['name'];
        $coach->email_address = $validatedData['email_address'];
        $coach->description = $validatedData['description'];
        $coach->save();

        return redirect()->route('admin.coaches')->with('success', 'Coach updated successfully');
    }

    public function deleteCoach($id)
    {
    $coach = Coach::find($id);

    if (!$coach) {
        return redirect()->route('admin.coaches')->with('error', 'Coach not found');
    }

    $coach->delete();

    return redirect()->route('admin.coaches')->with('success', 'Coach soft-deleted successfully');
    }
    

    public function restoreAllCoaches()
    {
        $softDeletedCoaches = Coach::onlyTrashed()->get();

        foreach ($softDeletedCoaches as $coach) {
            $coach->restore();
        }

        return redirect()->route('admin.coaches')->with('success', 'All coaches restored successfully');
    }
}
