<?php

// app/Http/Controllers/CoachController.php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Coach;

class CoachController extends Controller
{
    public function index()
    {
        $coaches = new Coach();
        $coaches = $coaches->all();
        return view('auth.coaches')->with('coaches', $coaches);
    }
}

