<?php

namespace App\Http\Controllers;

use App\Models\Recipes;
use App\Models\RecipeCategories;
use Illuminate\Http\Request;

class RecipesController extends Controller
{
    public function index()
    {
            $recipes = new Recipes();
            $recipe = $recipes->all();
            return view('auth.recipes')->with('recipes', $recipe);
       
    }

    public function recipes($shortname) {
        $recipes = new recipes();
        $recipe_category = RecipeCategories::where('shortname', '=', $shortname)->first(); 
        $recipes = recipes::where('category_id', '=', $recipe_category->category_id)->get();
        return view('auth.recipes')->with('recipes', $recipes)->with('recipe_category', $recipe_category);
    }
}
