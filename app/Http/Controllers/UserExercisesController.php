<?php

namespace App\Http\Controllers;

use App\Models\UserExercises;
use Illuminate\Http\Request;

class UserExercisesController extends Controller
{   
    public function saveexercise(Request $request) {
        if (isset($request->selected_exercises)) {
        foreach ($request->selected_exercises as $exercise_id) {
            UserExercises::create([
                'exercise_id' => $exercise_id,
                
                'user_id' => auth()->user()->id
            ]);
        }}
        return redirect()->route('myprogress');
    }
}
