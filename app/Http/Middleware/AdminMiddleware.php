<?php

namespace App\Http\Middleware;
use Illuminate\Support\Facades\Auth;
use Closure;

class AdminMiddleware
{
    public function handle($request, Closure $next)
    {
        if(is_null(Auth::user())){
            return redirect('/dashboard');        };
        if (Auth::user()->admin == 0) {
            return redirect('/dashboard');        }

        return $next($request);
    }
}