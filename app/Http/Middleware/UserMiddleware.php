<?php

namespace App\Http\Middleware;
use Illuminate\Support\Facades\Auth;
use Closure;

class UserMiddleware
{
    public function handle($request, Closure $next)
    {
        if(is_null(Auth::user())){
            return redirect('/dashboard');        };

        return $next($request);
    }
}