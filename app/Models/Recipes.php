<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Recipes extends Model
{
    use HasFactory;
    use SoftDeletes;
    protected $table = 'recipes';
    public $primaryKey = 'recipe_id';
    protected $fillable = [
        'name',
        'ingredients',
        'category_id',
        'image_url',
        'method',
        'calories_per_serving',
    ];
    public $timestamps = false;
    protected $dates = ['deleted_at'];
}
