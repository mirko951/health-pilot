<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserExercises extends Model
{
    use HasFactory;
    protected $table = 'userexercises';
    protected $fillable = ['exercise_id','user_id','rep'];
    public $timestamps = false;
}
