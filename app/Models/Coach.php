<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Coach extends Model
{
    use HasFactory;
    use SoftDeletes;
    // Define the one-to-many relationship with CoachReview model
    public function reviews()
    {
        return $this->hasMany(CoachReview::class, 'coach_id');
    }
    protected $table = 'Coaches'; // Nome della tabella nel database
    protected $primaryKey = 'id'; // Chiave primaria
    public $timestamps = false; // Disabilita i campi di data created_at e updated_at

    protected $fillable = [
        'name',
        'photo',
        'description',
        'email_address',
    ];
}
