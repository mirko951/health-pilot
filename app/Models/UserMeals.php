<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserMeals extends Model
{
    use HasFactory;
    protected $table = 'usermeals';
    protected $fillable = ['recipe_id','user_id'];
    public $timestamps = false;
}
