<?php
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Auth\LoginRegisterController;
use App\Http\Controllers\ExerciseCategoriesController;
use App\Http\Controllers\RecipesCategoriesController;
use App\Http\Controllers\ExercisesController;
use App\Http\Controllers\RecipesController;
use App\Http\Controllers\UserExercisesController;
use App\Http\Controllers\UserMealsController;
use App\Http\Controllers\MyAccountController;
use App\Http\Controllers\AboutUsController;
use App\Http\Controllers\StripeController;
use App\Http\Controllers\ProductsController;
use App\Http\Controllers\CoachController;
use App\Http\Controllers\CoachReviewController;
use App\Http\Controllers\AdminExercisesController;
use App\Http\Controllers\AdminRecipesController;
use App\Http\Controllers\AdminProductsController;
use App\Http\Controllers\AdminCoachesController;



/*ApApp\Http\Controllers\Controller
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// AUTHETICATION ROUTES
Route::controller(LoginRegisterController::class)->group(function() {
    Route::get('/register', 'register')->name('register');
    Route::post('/store', 'store')->name('store');
    Route::get('/login', 'login')->name('login');
    Route::post('/authenticate', 'authenticate')->name('authenticate');
    Route::get('/dashboard', 'dashboard')->name('dashboard');
    Route::post('/logout', 'logout')->name('logout');
    Route::get('/', 'dashboard')->name('dashboard');
});

Route::group(['middleware' => 'user'], function () 
{
    //WEBPAGE
    Route::get('/exerciseslist', [ExerciseCategoriesController::class, 'index'])->name('exerciseslist');
    Route::get('/recipeslist', [RecipesCategoriesController::class, 'index'])->name('recipeslist');
    Route::get('/exercises', [ExercisesController::class, 'index'])->name('exercises');
    Route::get('/recipes', [RecipesController::class, 'index'])->name('recipes');
    Route::get('/exercises/{shortname}', [ExercisesController::class, 'exercises'])->name('exercises');
    Route::post('/save-exercise', [UserExercisesController::class, 'saveexercise'])->name('save-exercise');
    Route::get('/recipes/{shortname}', [RecipesController::class, 'recipes'])->name('recipes');
    Route::post('/savemeals', [UserMealsController::class, 'store'])->name('savemeals');
    Route::get('/myaccount', [MyAccountController::class, 'index'])->name('myaccount');
    Route::get('/myprogress', [MyAccountController::class, 'myprogress'])->name('myprogress');
    Route::get('/myinformation', [MyAccountController::class, 'myinformation'])->name('myinformation');
    Route::get('/account', [MyAccountController::class, 'account'])->name('account');
    Route::get('/coach',[CoachController::class, 'index'])->name('coach');
    Route::get('/reviews/create/{coach_id}', [CoachReviewController::class, 'create'])->name('reviews.create');
    Route::post('/reviews', [CoachReviewController::class, 'store'])->name('reviews.store');

    //SHOPPING CART ROUTES
    Route::post('/session', [StripeController::class, 'session'])->name('session');
    Route::get('/success', [StripeController::class, 'success'])->name('success');
    Route::get('/cancel', [StripeController::class, 'cancel'])->name('cancel');
    Route::get('/products', [ProductsController::class, 'index'])->name('products');
    Route::get('/cart', [ProductsController::class, 'cart'])->name('cart');
    Route::get('/add-to-cart/{id}', [ProductsController::class, 'addToCart'])->name('add_to_cart');
    Route::patch('/update-cart', [ProductsController::class, 'update'])->name('update_cart');
    Route::delete('/remove-from-cart', [ProductsController::class, 'remove'])->name('remove_from_cart');
});
Route::get('/about-us',[AboutUsController::class, 'index'])->name('about_us');

// ADMIN ROUTES
Route::group(['middleware' => 'admin'], function () 
    { 
        Route::get('/admin/exercises', [AdminExercisesController::class, 'viewExercises'])->name('admin.exercises');
        Route::delete('/admin/delete-exercise/{exercise_id}', [AdminExercisesController::class, 'deleteExercise'])->name('delete-exercise');
        Route::put('/admin/restore-all-exercises', [AdminExercisesController::class, 'restoreAllExercises'])->name('restore-all-exercises');
        Route::get('/admin/create-exercise', [AdminExercisesController::class, 'createExerciseForm'])->name('create-exercise');
        Route::post('/admin/store-exercise', [AdminExercisesController::class, 'storeExercise'])->name('store-exercise');
        Route::get('/admin/edit-exercise/{exercise_id}', [AdminExercisesController::class, 'editExerciseForm'])->name('edit-exercise');
        Route::put('/admin/update-exercise/{exercise_id}', [AdminExercisesController::class, 'updateExercise'])->name('update-exercise');

        Route::get('/admin/recipes', [AdminRecipesController::class, 'viewRecipes'])->name('admin.recipes');
        Route::delete('/admin/delete-recipe/{recipe_id}', [AdminRecipesController::class, 'deleterecipe'])->name('delete-recipe');
        Route::put('/admin/restore-all-recipes', [AdminRecipesController::class, 'restoreAllrecipes'])->name('restore-all-recipes');
        Route::get('/admin/create-recipe', [AdminRecipesController::class, 'createrecipeForm'])->name('create-recipe');
        Route::post('/admin/store-recipe', [AdminRecipesController::class, 'storerecipe'])->name('store-recipe');
        Route::get('/admin/edit-recipe/{recipe_id}', [AdminRecipesController::class, 'editrecipeForm'])->name('edit-recipe');
        Route::put('/admin/update-recipe/{recipe_id}', [AdminRecipesController::class, 'updaterecipe'])->name('update-recipe');

        Route::get('/admin/create-product', [AdminProductsController:: class, 'createProductForm'])->name('admin.create-product');
        Route::post('/admin/store-product', [AdminProductsController:: class, 'storeProduct'])->name('admin.store-product');
        Route::get('/admin/edit-product/{id}', [AdminProductsController:: class, 'editProductForm'])->name('admin.edit-product');
        Route::put('/admin/update-product/{id}', [AdminProductsController:: class, 'updateProduct'])->name('admin.update-product');
        Route::delete('/admin/delete-product/{id}', [AdminProductsController:: class, 'deleteProduct'])->name('admin.delete-product');
        Route::get('/admin/products', [AdminProductsController:: class, 'viewProducts'])->name('admin.products');
        Route::put('/admin/restore-all-products', [AdminProductsController:: class, 'restoreAllProducts'])->name('admin.restore-all-products');

        Route::get('/admin/coaches', [AdminCoachesController::class, 'viewCoaches'])->name('admin.coaches');
        Route::get('/admin/create-coach', [AdminCoachesController::class, 'createCoachesForm'])->name('admin.create-coach');
        Route::post('/admin/store-coach', [AdminCoachesController::class, 'storeCoaches'])->name('admin.store-coach');
        Route::get('/admin/edit-coach/{id}', [AdminCoachesController::class, 'editCoachesForm'])->name('admin.edit-coach');
        Route::put('/admin/update-coach/{id}', [AdminCoachesController::class, 'updateCoaches'])->name('admin.update-coach');
        Route::delete('/admin/delete-coach/{id}', [AdminCoachesController:: class, 'deleteCoach'])->name('admin.delete-coach');
        Route::put('/admin/restore-all-coaches', [AdminCoachesController::class, 'restoreAllCoaches'])->name('admin.restore-all-coaches');
    });



