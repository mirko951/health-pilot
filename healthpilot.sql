-- phpMyAdmin SQL Dump
-- version 5.2.1
-- https://www.phpmyadmin.net/
--
-- Host: mysql
-- Creato il: Set 28, 2023 alle 12:16
-- Versione del server: 8.0.32
-- Versione PHP: 8.2.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `healthpilot`
--

-- --------------------------------------------------------

--
-- Struttura della tabella `Coaches`
--

CREATE TABLE `Coaches` (
  `id` int NOT NULL,
  `name` varchar(255) NOT NULL,
  `photo` varchar(255) DEFAULT NULL,
  `description` text,
  `email_address` varchar(255) DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dump dei dati per la tabella `Coaches`
--

INSERT INTO `Coaches` (`id`, `name`, `photo`, `description`, `email_address`, `deleted_at`) VALUES
(1, 'Coach1', 'resources/images/coaches/user.jpg', 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.', 'coach@domain.com', NULL),
(2, 'Coach2', 'resources/images/coaches/user.jpg', 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.', NULL, NULL),
(3, 'Coach3', 'resources/images/coaches/user.jpg', 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.', NULL, NULL),
(4, 'Coach4', 'resources/images/coaches/user.jpg', 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.', NULL, NULL),
(5, 'Coach5', 'resources/images/coaches/user.jpg', 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.', NULL, NULL),
(6, 'Coach6', 'resources/images/coaches/user.jpg', 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.', NULL, NULL),
(7, 'coach7', 'resources/images/coaches/user.jpg', 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.', NULL, '2023-09-28 09:43:21');

-- --------------------------------------------------------

--
-- Struttura della tabella `coach_reviews`
--

CREATE TABLE `coach_reviews` (
  `id` int NOT NULL,
  `user_id` int UNSIGNED NOT NULL,
  `coach_id` int NOT NULL,
  `rating` int NOT NULL,
  `comment` text,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dump dei dati per la tabella `coach_reviews`
--

INSERT INTO `coach_reviews` (`id`, `user_id`, `coach_id`, `rating`, `comment`, `created_at`, `updated_at`) VALUES
(1, 4, 1, 5, 'dsAS', '2023-09-20 14:44:47', '2023-09-20 14:44:47'),
(2, 4, 1, 3, 'ds<yxc', '2023-09-20 14:45:38', '2023-09-20 14:45:38'),
(3, 4, 7, 3, 'not good! :(', '2023-09-21 13:53:27', '2023-09-21 13:53:27'),
(4, 4, 3, 4, 'nice', '2023-09-22 22:53:56', '2023-09-22 22:53:56'),
(5, 4, 1, 5, 'sehr gut!', '2023-09-24 11:15:04', '2023-09-24 11:15:04'),
(6, 4, 2, 5, 'sehr gut.', '2023-09-28 09:22:22', '2023-09-28 09:22:22');

-- --------------------------------------------------------

--
-- Struttura della tabella `dailycalories`
--

CREATE TABLE `dailycalories` (
  `user_id` bigint UNSIGNED NOT NULL,
  `day` int NOT NULL,
  `calories_intake` float NOT NULL,
  `calories_burned` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dump dei dati per la tabella `dailycalories`
--

INSERT INTO `dailycalories` (`user_id`, `day`, `calories_intake`, `calories_burned`) VALUES
(1, 1, 100, 50);

-- --------------------------------------------------------

--
-- Struttura della tabella `exercisecategories`
--

CREATE TABLE `exercisecategories` (
  `category_id` int NOT NULL,
  `name` varchar(50) COLLATE utf8mb4_general_ci NOT NULL,
  `images` varchar(200) COLLATE utf8mb4_general_ci NOT NULL,
  `shortname` varchar(128) COLLATE utf8mb4_general_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dump dei dati per la tabella `exercisecategories`
--

INSERT INTO `exercisecategories` (`category_id`, `name`, `images`, `shortname`) VALUES
(1, 'abdominal', 'resources/images/exercises/ABS.png', 'abdominals'),
(2, 'arms', 'resources/images/exercises/arms.png', 'arms'),
(3, 'back/schoulders', 'resources/images/exercises/back.png', 'back-shoulders'),
(4, 'legs', 'resources/images/exercises/legs.png', 'legs');

-- --------------------------------------------------------

--
-- Struttura della tabella `exercises`
--

CREATE TABLE `exercises` (
  `exercise_id` int NOT NULL,
  `category_id` int NOT NULL,
  `name` varchar(100) COLLATE utf8mb4_general_ci NOT NULL,
  `image_url` varchar(200) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_general_ci,
  `rep` varchar(128) COLLATE utf8mb4_general_ci NOT NULL,
  `calories` int NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dump dei dati per la tabella `exercises`
--

INSERT INTO `exercises` (`exercise_id`, `category_id`, `name`, `image_url`, `description`, `rep`, `calories`, `deleted_at`) VALUES
(1, 1, 'Jack Knife', 'resources/images/exercises/jack-knife.gif', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', '8 X 3', 88, NULL),
(2, 1, 'plank', 'resources/images/exercises/plank.gif', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', '60 seconds', 41, NULL),
(3, 2, 'One Arm Tricep Extension (seated)', 'resources/images/exercises/tricepseated.gif', 'How to: Hold a dumbbell in one hand directly behind your head, with your elbow bent and pointed towards the ceiling. Extend through your elbow until your arm is straight and the dumbbell is directly above you. Lower your arm back to starting position and repeat.', '3 X 10', 60, NULL),
(4, 2, 'Tate Press', 'resources/images/exercises/tatepress.gif', 'How to: Lie on a bench and hold two dumbbells directly above your shoulders. Slowly bend your elbows to bring the dumbbells to your chest, so your palms face outwards and the dumbbells point towards the ceiling. Raise the dumbbells back to starting position and repeat.', '10 X 3', 81, NULL),
(5, 2, ' Concentration Curl', 'resources/images/exercises/concentratedcurl.gif', 'Sit down on the bench and rest your right arm against your right leg, letting the weight hang down. Curl the weight up, pause, and then lower. Repeat with the other arm.\r\n', '15 X 3', 98, NULL),
(6, 2, 'Close-Grip Bench Press', 'resources/images/exercises/close-grip-benchpress.gif', 'How to: Grasp a barbell with an overhand grip that’s shoulder-width apart, and hold it above your sternum with arms completely straight. Lower the bar straight down, pause, and then press the bar back up to the starting position.', '10 X 8', 74, NULL),
(7, 2, 'Tricep Kickbacks\r\n', 'resources/images/exercises/tricep-kickbacks.gif', 'How to: Start standing and hinge at the hips, sending your hips behind your heels with a flat back and your arms bent, elbows pinned to your waist. Keep the elbows in place and extend the arms in a straight line behind you. Contract the triceps for a beat. Slowly resist the return of the dumbbells back to their beginning position. Avoid momentum; no cheating the reps.', '12 X 3', 78, NULL),
(8, 1, ' Cocoon', 'resources/images/exercises/cocoon.gif', 'Lay flat on your back with your arms extended behind your head and your feet slightly off the ground.\r\nPull your knees towards your chest, lift your backside off the floor and lift your arms over your head as you perform a crunch and repeat.', '10 X 3', 62, NULL),
(9, 1, 'Barbell Roll Outs', 'resources/images/exercises/rollouts.gif', 'Load a barbell with 10kg plates and grab the bar with an overhand, shoulder-width grip.\r\nPosition your shoulders directly over the barbell and slowly roll the bar forwards.\r\nPause, then reverse the move.\r\nRoll out to a distance that\'s challenging, but doesn\'t force your hips to sag.', '12 X 3', 86, NULL),
(10, 3, 'Bent-Over Dumbbell Alternating Row', 'resources/images/exercises/dumbbellalternate.gif', 'Grab a pair of dumbbells, hinge at your hips and knees, and lower your torso until it’s almost parallel to the floor. Your feet should be shoulder-width apart, and your lower back should be naturally arched; just make sure to avoid rounding your lower back.\r\nLet the dumbbells hang at arm’s length from your shoulders with your palms facing each other.\r\nKeeping your position, lift one dumbbell to your side, pause at the top of the movement, and slowly lower it. Then repeat with your other arm.', '3 X 8', 58, NULL),
(11, 3, 'Band Bent-Over Row', 'resources/images/exercises/bandbentoverrow.gif', 'Grab a low-resistance band and set it out on the ground.\r\nStand on the middle of the band, grabbing the two ends in either hand with a pronated (overhand) grip, hinging at the hips and slightly bending your knees in an athletic stance. Make sure that your back isn\'t rounded.\r\nSqueeze your back to pull the band ends simultaneously to your chest, or as close as the band allows.\r\nPause for a moment at the top of the motion, then slowly return to the original position, working against the band\'s resistance.', '4 X 15', 97, NULL),
(12, 3, 'Dumbbell front raise', 'resources/images/exercises/dumbbellfrontraise.gif', 'Standing, hold dumbbells in front of you with your palms facing your legs. Keep your elbows and knees slightly bent as you raise your arms straight in front of you to shoulder level. Slowly return to the starting position.', '4 X 12', 72, NULL),
(13, 3, ' Reverse fly', 'resources/images/exercises/reversefly.gif', 'Sitting, hold a dumbbell in each hand, and raise both weights to shoulder level with palms facing out and elbows bent. Press the weights up and toward each other as you straighten your arms. At the top of the movement, keep a slight bend in your elbows. Slowly bring down the weights, and return to the starting position.', '3 X 12', 84, NULL),
(14, 3, 'Plank dumbbell shoulder raise', 'resources/images/exercises/plankshoulder.gif', 'Holding the dumbbells, start in a plank position supported on your hands and on your toes. Begin with your feet a little bit wider than shoulder width. Move your feet in if you need to increase difficulty. From this position, alternate reps lifting one arm off the ground and straight out in front of you until it lines up with your body (parallel to the ground).', '3 X 5', 81, NULL),
(15, 4, 'Squat', 'resources/images/exercises/barbellsquat.gif', 'Stand with your feet shoulder-width apart and position the barbell on your upper back, resting it on your trapezius muscles.\r\nGrasp the bar with your hands slightly wider than shoulder-width apart, and keep your elbows pointing down towards the ground.\r\nTake a deep breath, brace your core, and begin to squat down by pushing your hips back and bending your knees.\r\nKeep your chest up, your back straight, and your heels firmly planted on the ground.\r\nLower your body until your thighs are parallel to the ground, or as low as you can comfortably go while maintaining proper form.\r\nDrive through your heels and push your body back up to the starting position, exhaling as you stand up.', '4 X 10', 95, NULL),
(16, 4, 'Sumo deadlift', 'resources/images/exercises/sumodeadlift.gif', NULL, '4 X 10', 84, NULL),
(17, 4, 'Dumbbell bulgarian split squat', 'resources/images/exercises/dumbbellbulgariansquat.gif', 'Begin by standing in a split stance with one foot positioned forward and the other foot positioned behind you, resting on a bench or elevated platform. The forward foot should be a comfortable distance away from the bench, ensuring that the knee of the forward leg doesn’t extend beyond the toes when you lunge down.\r\nHold a dumbbell in each hand, allowing your arms to hang by your sides.\r\nKeep your chest up, shoulders back, and core engaged throughout the exercise to maintain stability.\r\nInitiate the movement by bending your knees and lowering your body down, keeping the majority of your weight on the front leg.\r\nDescend until your front thigh is parallel to the ground, or as low as your flexibility allows, while maintaining proper form and balance.\r\nPause for a brief moment at the bottom of the movement, then push through the heel of your front foot to drive yourself back up to the starting position.\r\nRepeat the movement for the desired number of repetitions, then switch the position of your legs and repeat with the opposite leg forward.', '4 X 10', 88, NULL),
(18, 4, 'Leg curl', 'resources/images/exercises/legcurl.gif', NULL, '4 X 12', 78, NULL),
(19, 4, 'Hack squat calf raise', 'resources/images/exercises/calfraise.gif', NULL, '5 X 12', 91, NULL),
(20, 2, 'DUMBBELL HAMMER CURLS', 'resources/images/exercises/hammercurl.gif', 'How to: Stand with your feet flat on the floor, pointing straight ahead.\r\n\r\nHold a dumbbell in each hand with arms at your sides; palms facing each other.\r\n\r\nPerform a hammer curl by performing elbow flexion while keeping your palms facing each other.\r\n\r\nKeep your shoulder blades retracted throughout the exercise.\r\n\r\nSlowly return dumbbells to their original position.', '4 X 12', 49, NULL),
(21, 2, 'BENCH DUMBBELL TRICEPS EXTENSIONS', 'resources/images/exercises/Lying-Dumbbell-Triceps-Extension.gif', 'How to: Lie on a flat bench.\r\nYour feet should be flat with toes pointing straight ahead.\r\nHold dumbbells in both hands with elbows flexed.\r\nExtend your elbows until your arms are straight.\r\nHold.\r\nSlowly lower each dumbbell toward your forehead by flexing your elbows. Be sure to keep your low-back in a neutral position throughout the exercise. Do not let it excessively arch off the bench.\r\nRepeat.', '5 X 8', 53, NULL);

-- --------------------------------------------------------

--
-- Struttura della tabella `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint UNSIGNED NOT NULL,
  `uuid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struttura della tabella `migrations`
--

CREATE TABLE `migrations` (
  `id` int UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dump dei dati per la tabella `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_reset_tokens_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1),
(4, '2019_12_14_000001_create_personal_access_tokens_table', 1);

-- --------------------------------------------------------

--
-- Struttura della tabella `password_reset_tokens`
--

CREATE TABLE `password_reset_tokens` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struttura della tabella `personal_access_tokens`
--

CREATE TABLE `personal_access_tokens` (
  `id` bigint UNSIGNED NOT NULL,
  `tokenable_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tokenable_id` bigint UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `abilities` text COLLATE utf8mb4_unicode_ci,
  `last_used_at` timestamp NULL DEFAULT NULL,
  `expires_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struttura della tabella `products`
--

CREATE TABLE `products` (
  `id` int NOT NULL,
  `product_name` varchar(255) COLLATE utf8mb4_general_ci NOT NULL,
  `product_description` text COLLATE utf8mb4_general_ci,
  `photo` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `price` varchar(255) COLLATE utf8mb4_general_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dump dei dati per la tabella `products`
--

INSERT INTO `products` (`id`, `product_name`, `product_description`, `photo`, `price`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'T-shirt', 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.', 'resources/images/products/blackshirt.jpg', '12', '2023-09-19 17:57:04', '2023-09-25 19:05:43', NULL),
(2, 'Gloves', 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.', 'resources/images/products/gloves.jpg', '8', '2023-09-19 19:32:36', '2023-09-22 22:36:40', NULL),
(4, 'Water Bottle', 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.', 'resources/images/products/water-bottle.jpg', '7', '2023-09-22 08:07:17', '2023-09-22 22:36:40', NULL),
(5, 'Towel', 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.', 'resources/images/products/towel.jpg', '7', '2023-09-22 08:08:21', '2023-09-22 12:07:23', NULL),
(6, 'Dumbbells', 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.', 'resources/images/products/dumbbell.jpg', '27', '2023-09-22 08:09:57', '2023-09-22 08:19:22', NULL),
(7, 'Portable Multifunctional Push-up Board', 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.', 'resources/images/products/push-up.jpg', '12', '2023-09-22 08:11:47', '2023-09-22 12:07:32', NULL),
(8, 'Tracksuit', 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.', 'resources/images/products/tracksuit.jpg', '19', '2023-09-22 08:14:59', '2023-09-23 11:10:31', NULL);

-- --------------------------------------------------------

--
-- Struttura della tabella `recipecategories`
--

CREATE TABLE `recipecategories` (
  `category_id` int NOT NULL,
  `name` varchar(50) COLLATE utf8mb4_general_ci NOT NULL,
  `images` varchar(200) COLLATE utf8mb4_general_ci NOT NULL,
  `shortname` varchar(128) COLLATE utf8mb4_general_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dump dei dati per la tabella `recipecategories`
--

INSERT INTO `recipecategories` (`category_id`, `name`, `images`, `shortname`) VALUES
(1, 'breakfast', 'resources/images/recipes/breakfast.png', 'breakfast'),
(2, 'snack', 'resources/images/recipes/snack.jpg', 'snack'),
(3, 'lunch', 'resources/images/recipes/lunch.jpg', 'lunch'),
(4, 'dinner', 'resources/images/recipes/healthydinner.JPEG', 'dinner');

-- --------------------------------------------------------

--
-- Struttura della tabella `recipes`
--

CREATE TABLE `recipes` (
  `recipe_id` int NOT NULL,
  `category_id` int NOT NULL,
  `name` varchar(100) COLLATE utf8mb4_general_ci NOT NULL,
  `image_url` varchar(200) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `ingredients` text COLLATE utf8mb4_general_ci,
  `method` text COLLATE utf8mb4_general_ci NOT NULL,
  `calories_per_serving` int NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dump dei dati per la tabella `recipes`
--

INSERT INTO `recipes` (`recipe_id`, `category_id`, `name`, `image_url`, `ingredients`, `method`, `calories_per_serving`, `deleted_at`) VALUES
(1, 1, 'Healthy homemade granola', 'resources/images/recipes/granola.JPEG', 'Ingredients:\r\n290g can pitted prunes in natural juice, drained, 1 orange, zested and juiced, 2 tbsp tahini, 350g oats, 25g flaked almonds, 25g sunflower seeds, 25g pumpkin seeds, 2 x 400g pots fortified soya or plain bio yogurt.', 'Method: Heat the oven to 200C/180C fan/gas 6 and line a large baking tray with baking parchment. Put the prunes, orange zest and juice in a small bowl, add the tahini and mash it all together to make a paste. Tip the oats into a large bowl, then add the prune mixture and knead it all together using your hands, as though you’re making a crumble topping, until all the oats are coated and sticky. Spread out on the prepared tray and bake for 20 mins, turning the oats every 5 mins to help them cook evenly and drive off as much steam as possible.Remove from the oven and stir the almonds and seeds onto the tray, then cool quickly by tossing the mixture. Once completely cooled, the granola will keep for two weeks in an airtight container.Measure 50g granola per serving and enjoy with 100g soya yogurt.', 362, NULL),
(2, 1, 'Overnight oats', 'resources/images/recipes/overnightoats.JPEG', 'Inredients:\r\n¼ tsp ground cinnamon, 50g rolled porridge oats, 2 tbsp natural yogurt, 50g mixed berries, drizzle of honey, ½ tbsp nut butter.\r\n\r\n', 'Method:\r\nThe night before serving, stir the cinnamon and 100ml water (or milk) into your oats with a pinch of salt.\r\nThe next day, loosen with a little more water (or milk) if needed. Top with the yogurt, berries, a drizzle of honey and the nut butter.', 348, NULL),
(13, 1, 'Breakfast super-shake', 'resources/images/recipes/supershake.JPEG', 'Ingredients: \r\n100ml full-fat milk, \r\n2 tbsp natural yogurt, \r\n1 banana, \r\n150g frozen fruits of the forest, \r\n50g blueberries, \r\n1 tbsp chia seeds, \r\n½ tsp cinnamon, \r\n1 tbsp goji berries, \r\n1 tsp mixed seeds, \r\n1 tsp honey (ideally Manuka).', 'Method: \r\nPut the ingredients in a blender and blitz until smooth. Pour into a glass and enjoy!', 391, NULL),
(14, 1, 'Vegan strawberry pancakes', 'resources/images/recipes/strawberrypancakes.JPEG', 'Ingredients: \r\n115g wholemeal spelt flour, \r\n1 tsp baking powder, \r\n1 tsp cinnamon, \r\n150ml soya milk, \r\n240g soya yogurt, \r\n1 tsp vanilla extract, \r\ndrop of rapeseed oil, \r\n200g strawberries, hulled and halved or quartered if large, \r\n2 tbsp chopped pecans, \r\na few small mint leaves, optional.\r\n', 'Method: \r\nMix the flour with the baking powder and cinnamon in a bowl using a balloon whisk. In a jug, whisk together the soya milk, 2 tbsp of the yogurt and vanilla extract, then whisk this into the dry ingredients to make a thick batter. \r\nRub the oil around the pan using kitchen paper, then set the pan over a medium heat. Spoon in 1½ tbsp batter in three or four places to make small pancakes. Cook over a low heat for 1-2 mins until set, and bubbles appear on the surface, then turn the pancakes using a palette knife. Cook for another 1-2 mins until golden and cooked through. Repeat with the remaining batter to make six pancakes in total. \r\nServe three pancakes per person topped with the remaining yogurt, berries, pecans and mint leaves.', 453, NULL),
(15, 1, 'Banana & tahini porridge\r\n', 'resources/images/recipes/porridge.JPEG', 'Ingredients: \r\n1 tbsp tahini, \r\n150ml milk of your choice, plus 1 tbsp, \r\n100g porridge oats, \r\n2 small bananas, sliced,  \r\nseeds from 2 cardamom pods, crushed, \r\n1 tbsp toasted sesame seeds.', 'Method: \r\nMix the tahini with 1 tbsp milk and 1 tbsp water. Put the oats, 1 sliced banana, cardamom, 100ml milk and 300ml water in a pan with a pinch of salt. Cook over a medium heat for 5 mins, stirring, until creamy and hot. \r\nDivide between two bowls. Pour over the remaining milk, then top with the remaining sliced banana. Drizzle over the tahini mixture and sprinkle over the toasted sesame seeds.', 431, NULL),
(16, 2, 'Avocado smoothie', 'resources/images/recipes/avocadoshake.JPEG', 'Ingredients: \r\n½ avocado, peeled, stoned and roughly chopped, \r\ngenerous handful spinach, \r\ngenerous handful kale, washed well, \r\n50g pineapple chunks, \r\n10cm piece cucumber, roughly chopped, \r\n300ml coconut water.', 'Method: \r\nPut the avocado, spinach, kale, pineapple and cucumber in the blender. \r\nTop up with coconut water, then blitz until smooth.', 67, NULL),
(17, 2, 'Watermelon salsa\r\n', 'resources/images/recipes/watermelon-salsa.jpg', 'Ingredients: \r\n200g watermelon, \r\n2 small shallots, \r\nsmall bunch coriander, \r\njuice 1/2 lime, \r\n2 tbsp olive oil.', 'Method: \r\nFinely chop 200g watermelon, 2 small shallots and a small bunch coriander. Mix together with juice ½ lime and 2 tbsp olive oil. Season and serve as a dip or pile on top of cheesy nachos.', 46, NULL),
(18, 2, 'Apricot & seed protein bar', 'resources/images/recipes/apricot-bar.jpg', 'Ingredients: \r\n140g dried apricot, \r\n40g oats, \r\n40g desiccated coconut, \r\n25g sunflower seed, \r\n1 tbsp sesame seeds, \r\n15g dried cranberries, \r\n3 tbsp hemp protein powder, \r\n1 tbsp chia seeds.', 'Method: \r\nPurée apricots in a food processor with 150ml boiling water and the oats, then scrape the mixture into a bowl. Toast coconut, sunflower seeds and sesame seeds in a non-stick pan over a low heat, then stir into the apricots with the cranberries, hemp powder and chia seeds to make a thick paste. \r\nRoll into a long log on a sheet of cling film. Wrap tightly, chill, then slice thinly to serve. Will keep in the fridge for 2 weeks.', 78, NULL),
(19, 2, 'Healthy cookies', 'resources/images/recipes/cookies.jpg', 'Ingredients: \r\n2 ripe bananas, mashed, \r\n150g porridge oats, \r\n2 tbsp ground almonds, \r\n1⁄2 tsp cinnamon, \r\n100g raspberries (fresh or frozen).', 'Method: \r\nHeat the oven to 200C/180C fan/gas 4 and line two baking trays with baking parchment. Mix the banana, oats, almonds, cinnamon and a pinch of salt in a bowl to make a sticky dough. Gently stir through the raspberries, trying not to break them up. Scoop up tablespoons of the mixture and roll into balls, then place on a baking tray and flatten with your hand. \r\nBake for 15 mins until the cookies feel firm around the edges and are golden brown. Leave to cool. Will keep in an airtight container for up to three days.', 51, NULL),
(20, 2, 'Baked carrot & nigella seed bhajis with raita', 'resources/images/recipes/baked-carrot-nigella-seed-bhaji.jpg', 'Ingredients: \r\n100g gram flour (chickpea flour), \r\n1 tsp ground turmeric, \r\n2 tsp nigella seeds, \r\n½ tsp ground cumin, \r\n½ tsp ground coriander, \r\n½ tsp ground ginger, \r\n½ tsp chilli powder, \r\n2 large eggs, \r\n4 large carrots (about 400g), peeled,  ends trimmed and spiralized into thin noodles, \r\n2 tsp vegetable oil. \r\nFor the raita: \r\n½ cucumber, grated, \r\n150g pot of natural yogurt, \r\n½ small pack of mint, leaves finely , chopped.', 'Method: \r\nHeat oven to 200C/180C fan/gas 6.Line one large or two medium baking trays with baking parchment. \r\nMix all the ingredients for the carrot bhajis, apart from the carrot and oil, together in a large bowl to form a thick batter. If the mixture looks a little dry add a splash of water. Stir in the sprialized carrots, cutting any large spirals in half, and season. \r\nDollop 12 spoonfuls of the mixture onto the baking tray, leaving enough space to flatten the bhajis with the back of a spoon. Drizzle over the oil and bake for 25 mins or until golden brown, flipping the bhajis halfway. \r\nWhile the bhajis are cooking, squeeze any excess moisture from the cucumber using a tea towel then combine all the ingredients for the raita together in a small bowl, seasoning to taste. Serve alongside the baked bhajis.', 72, NULL),
(21, 3, 'Burrito bowl with chipotle black beans', 'resources/images/recipes/burritobowl.JPEG', 'Ingredients: \r\n125g basmati rice, \r\n1 tbsp olive oil, \r\n2 garlic cloves, chopped, \r\n400g can black beans, drained and rinsed, \r\n1 tbsp cider vinegar, \r\n1 tsp honey, \r\n1 tbsp chipotle paste, \r\n100g chopped curly kale, \r\n1 avocado, halved and sliced, \r\n1 medium tomato, chopped, \r\n1 small red onion, chopped.  \r\nTo serve (optional): \r\nchipotle hot sauce, \r\ncoriander leaves, \r\nlime wedges.', 'Method\r\nCook the rice following pack instructions, then drain and return to the pan to keep warm. In a frying pan, heat the oil, add the garlic and fry for 2 mins or until golden. Add the beans, vinegar, honey and chipotle. Season and warm through for 2 mins. \r\nBoil the kale for 1 min, then drain, squeezing out any excess water. Divide the rice between big shallow bowls and top with the beans, kale, avocado, tomato and onion. Serve with hot sauce, coriander and lime wedges, if you like.', 573, NULL),
(22, 3, 'Healthy pesto eggs on toast', 'resources/images/recipes/Healthy-pesto-eggs-on-toast.jpg', 'Ingredients: \r\n2-4 thin slices rye sourdough (about 70g total, depending on the size of the loaf), \r\n2 eggs, \r\n170g tomatoes on-the-vine, \r\n160g baby spinach, \r\npinch of chilli flakes (optional). \r\nFor the pesto:\r\n1 garlic clove, \r\n10g basil, \r\n1 tbsp pine nuts, \r\n1 tbsp rapeseed oil, \r\n1 tbsp finely grated parmesan or vegetarian alternative.\r\n', 'Method: \r\nTo make the pesto, peel the garlic clove and put in a small food processor along with the basil, pine nuts, oil and 2 tbsp water. Blitz until smooth, then stir in the cheese. Or, blitz using a hand blender. \r\nToast the bread and divide between two plates. Cook the pesto in a frying pan over a medium heat for 30 seconds, stirring. Crack the eggs into one side of the pan, put the tomatoes in the other, and fry in the pesto until the eggs are cooked to your liking. \r\nLift out the eggs and put each one on a slice of toast. Add the spinach to the pan with the tomatoes, turn the heat up to high and cook until wilted, about 2-3 mins. The tomatoes should be soft. Spoon the veg onto the other toast slice and sprinkle with the chilli flakes, if you like.', 287, NULL),
(23, 4, 'Spiced carrot & lentil soup\r\n', 'resources/images/recipes/carrotslentilssoup.JPEG', 'Ingredients: \r\n2 tsp cumin seeds, \r\npinch chilli flakes, \r\n2 tbsp olive oil, \r\n600g carrots, washed and coarsely grated (no need to peel), \r\n140g split red lentils, \r\n1l hot vegetable stock (from a cube is fine), \r\n125ml milk, \r\nplain yogurt and naan bread, to serve. \r\n', 'Method: \r\nHeat a large saucepan and dry-fry 2 tsp cumin seeds and a pinch of chilli flakes for 1 min, or until they start to jump around the pan and release their aromas.\r\nScoop out about half with a spoon and set aside. Add 2 tbsp olive oil, 600g coarsely grated carrots, 140g split red lentils, 1l hot vegetable stock and 125ml milk to the pan and bring to the boil.\r\nSimmer for 15 mins until the lentils have swollen and softened.\r\nWhizz the soup with a stick blender or in a food processor until smooth (or leave it chunky if you prefer).\r\nSeason to taste and finish with a dollop of plain yogurt and a sprinkling of the reserved toasted spices. Serve with warmed naan breads.', 391, NULL),
(24, 4, 'Devilled tofu kebabs', 'resources/images/recipes/tofukebap.JPEG', 'Ingredients: \r\n8 shallots or button onions, \r\n8 small new potatoes, \r\n2 tbsp tomato purée, \r\n2 tbsp light soy sauce, \r\n1 tbsp sunflower oil,\r\n1 tbsp clear honey, \r\n1 tbsp wholegrain mustard, \r\n300g firm smoked tofu, cubed, \r\n1 courgette, peeled and sliced, \r\n1 red pepper, deseeded and diced.', 'Method: \r\nPut the shallots or button onions in a bowl, cover with boiling water and set aside for 5 mins. Cook the potatoes in a pan of boiling water for 7 mins until tender. Drain and pat dry. Put tomato purée, soy sauce, oil, honey, mustard and seasoning in a bowl, then mix well. Toss the tofu in the marinade. Set aside for at least 10 mins.\r\nHeat the grill. Drain and peel shallots or onions, then cook in boiling water for 3 mins. Drain well. Thread the tofu, shallots, potatoes, courgette and pepper on to 8 x 20cm skewers. Grill for 10 mins, turning frequently and brushing with remaining marinade before serving.', 178, '2023-09-28 09:43:37'),
(25, 2, 'Instant frozen berry yogurt\r\n', 'resources/images/recipes/instant-frozen-berry-yogurt.jpg', 'Ingredients: 250g frozen mixed berry, \r\n250g 0%-fat Greek yogurt, \r\n1 tbsp honey or agave syrup. ', 'Method: Blend berries, yogurt and honey or agave syrup in a food processor for 20 seconds, until it comes together to a smooth ice-cream texture. Scoop into bowls and serve.', 71, NULL),
(26, 2, 'Choc-orange energy balls', 'resources/images/recipes/Choc-orange-energy-balls.jpg', 'Ingredients: \r\n100g pitted medjool dates, \r\n100g pecan nuts, \r\n50g pumpkin seeds, \r\n50g rolled oats, \r\n4 tbsp cacao powder or unsweetened cocoa, \r\n2 heaped tbsp almond butter, \r\nzest and juice 1 orange. ', 'Method:\r\nPlace all the ingredients and 3 tbsp orange juice in a food processor. Blitz until chopped and starting to clump together. If it’s a bit dry, add a drop more orange juice. Roll the mixture into walnut-sized balls with lightly oiled hands. Pop 2 or 3 into a lunchbox for a snack. Keeps in a sealed container for 2 weeks in the fridge.', 99, NULL),
(29, 1, 'Coriander juice', 'resources/images/recipes/Coriander-juice.jpg', 'Ingredients:\r\n50g coriander, \r\n½ lemon, \r\n5g ginger, \r\n½ cucumber, \r\n1 tsp maple syrup. ', 'Method: \r\nTip the coriander, lemon juice, ginger, cucumber and maple syrup along with 200ml water and a pinch of salt into a high-powered blender. Blend for 1-2 mins until very smooth, scraping down the sides a few times. Strain through a fine sieve if you like and serve over ice.', 53, NULL),
(30, 1, 'Breakfast egg wraps', 'resources/images/recipes/breakfast-egg-wraps.jpg', 'Ingredients: \r\n500g pack closed cup mushrooms, \r\n4 tsp rapeseed oil, plus 2 drops, \r\n320g cherry tomatoes, halved, or 8 tomatoes, cut into wedges,\r\n2 generous handfuls parsley, finely chopped, \r\n8 tbsp porridge oats (40g), \r\n10 eggs, \r\n4 tsp English mustard powder made up with water. ', 'Method:\r\nThickly slice half the pack of mushrooms. Heat 2 tsp rapeseed oil in a non-stick pan. Add the mushrooms, stir briefly then fry with the lid on the pan for 6-8 mins. Stir in half the tomatoes then cook 1-2 mins more with the lid off until softened.\r\n\r\nBeat together the eggs really well with the parsley and oats. Heat a drop of oil in a large non-stick frying pan. Pour in a ¼ of the egg mix and fry for 1 min until almost set, flip over as if making a pancake. Tip from the pan, spread with a quarter of the mustard, spoon a ¼ the filling down the centre and roll up. Now make a second wrap using another ¼ of the egg mix and filling. If you\'re following our Healthy Diet Plan, save the rest for the following day.', 429, NULL),
(31, 3, 'BLT pasta salad', 'resources/images/recipes/blt-pasta-salad.jpg', 'Ingredients: \r\n25g pasta bows, \r\n2 cooked crispy bacon rashers, broken into pieces, \r\n15g spinach, chopped, \r\n6 cherry tomatoes, halved, \r\n½ tbsp crème fraîche, \r\n¼ tsp wholegrain mustard. ', 'Method:\r\nThe night before school, cook the pasta following pack instructions and run under cold water to cool quickly. Mix in the bacon, spinach, tomatoes, crème fraîche and mustard, and season with a little salt. Spoon into an airtight container and keep overnight in the fridge.\r\n\r\n', 322, NULL),
(32, 3, 'Halloumi, carrot & orange salad', 'resources/images/recipes/halloumi-carrot-orange-salad.jpg', 'Ingredients:\r\n2 large oranges, \r\n1½ tbsp wholegrain mustard, \r\n1½ tsp honey, \r\n1 tbsp white wine vinegar, \r\n3 tbsp rapeseed or olive oil, plus extra for frying, \r\n2 large carrots, peeled, \r\n225g block halloumi, sliced, \r\n100g bag watercress or baby spinach.  \r\n', 'Method:\r\nCut the peel and pith away from the oranges. Use a small serrated knife to segment the orange, catching any juices in a bowl, then squeeze any excess juice from the off-cut pith into the bowl as well. Add the mustard, honey, vinegar, oil and some seasoning to the bowl and mix well.\r\nUsing a vegetable peeler, peel carrot ribbons into the dressing bowl and toss gently. Heat a drizzle of oil in a frying pan and cook the halloumi for a few mins until golden on both sides. Toss the watercress through the dressed carrots. Arrange the watercress mixture on plates and top with the halloumi and oranges.', 338, NULL),
(33, 3, 'Beetroot soup', 'resources/images/recipes/beetroot-onion-seed-soup.jpg', 'Ingredients:\r\n250g cooked beetroot, \r\n100g canned lentils, \r\n1 small apple, \r\n1 crushed garlic clove, \r\n1 tsp onion seeds (nigella), plus extra to serve, \r\n250ml vegetable stock. ', 'Method:\r\nTip the beetroot, lentils, apple, garlic and onion seeds into a blender with the vegetable stock and some seasoning, and blitz until smooth. Heat until piping hot in the microwave or on the hob, then scatter over some extra onion seeds, if you like.', 257, NULL),
(34, 3, 'Easy prawn & quinoa salad', 'resources/images/recipes/asian-prawn-quinoa-salad.jpg', 'Ingredients:\r\n60g quinoa, \r\n150g cooked shelled prawns, \r\n1 small avocado, stoned and sliced, \r\n¼ cucumber, halved and sliced, \r\n50g watercress, \r\n100g cherry tomatoes, halved, \r\nfinely grated zest and juice 1 large , lime, \r\n1 red chilli, deseeded and finely chopped, \r\n2 spring onions, trimmed and finely chopped, \r\n1 tsp wheat-free tamari, \r\nhandful coriander, chopped, \r\n1 tsp rapeseed oil, \r\n½ tsp maple syrup. ', 'Method:\r\nBoil the quinoa in a small pan for 15 mins until the grains are tender and look like they have burst. Drain well and tip into a bowl. Meanwhile, make the dressing: mix the lime zest and juice and the chilli in a bowl.\r\nStir half the dressing into the quinoa with the spring onions, tamari and half the coriander. Stir in all the salad vegetables, then spoon onto two serving plates.\r\nStir the oil and maple syrup into the remaining dressing and toss in the prawns. Spoon onto the quinoa salad and scatter over the coriander to serve.', 305, NULL),
(35, 3, 'Bombay potato frittata', 'resources/images/recipes/bombay-potato-frittata.jpg', 'Ingredients:\r\n4 new potatoes, sliced into 5mm rounds, \r\n100g baby spinach, chopped, \r\n1 tbsp rapeseed oil, \r\n1 onion, halved and sliced, \r\n1 large garlic clove, finely grated, \r\n½ tsp ground coriander, \r\n½ tsp ground cumin, \r\n¼ tsp black mustard seeds, \r\n¼ tsp turmeric, \r\n3 tomatoes, roughly chopped, \r\n2 large eggs, \r\n½ green chilli, deseeded and finely chopped, \r\n1 small bunch of coriander, finely chopped, \r\n1 tbsp mango chutney, \r\n3 tbsp fat-free Greek yogurt. ', 'Method: \r\nCook the potatoes in a pan of boiling water for 6 mins, or until tender. Drain and leave to steam-dry. Meanwhile, put the spinach in a heatproof bowl with 1 tbsp water. Cover and microwave for 3 mins on high, or until wilted.\r\nHeat the rapeseed oil in a medium non-stick frying pan. Add the onion and cook over a medium heat for 10 mins until golden and sticky. Stir in the garlic, ground coriander, ground cumin, mustard seeds and turmeric, and cook for 1 min more. Add the tomatoes and wilted spinach and cook for another 3 mins, then add the potatoes.\r\nHeat the grill to medium. Lightly beat the eggs with the chilli and most of the fresh coriander and pour over the potato mixture. Grill for 4-5 mins, or until golden and just set, with a very slight wobble in the middle.\r\nLeave to cool, then slice into wedges. Mix the mango chutney, yogurt and remaining fresh coriander together. Serve with the frittata wedges.', 317, NULL),
(36, 4, 'Pork & bulgur-stuffed peppers', 'resources/images/recipes/stuffed-peppers.jpg', 'Ingredients: \r\n4 peppers, halved and cores removed, \r\n200g minced pork, \r\n1 garlic clove, crushed, \r\n2 tsp ground cumin, \r\n1 tsp paprika, \r\n50g bulgur wheat, \r\n250ml vegetable stock, \r\n½ small bunch parsley, chopped, \r\n4 tbsp 0% Greek yogurt, to serve. ', 'Method: \r\nPut the peppers, cut-side down, on a plate and microwave on High for 4 mins until cooked through (but not so soft they collapse). If they need longer, microwave for 1 min more and repeat until done.\r\nPut the pork in a cold frying pan and turn on the heat. Fry, breaking up any lumps, until it starts to brown. Stir in the garlic and spices for 1 min, then add the bulgur and stock. Cover and simmer for 10 mins until the bulgur is soft.\r\nHeat the grill. Stir half the parsley into the bulgur, then stuff into the peppers on a baking tray. Grill to crisp, sprinkle over most of the parsley, then serve with the yogurt mixed with remaining parsley.', 185, NULL),
(37, 4, 'Spicy bean burgers with lime yogurt & salsa', 'resources/images/recipes/spicy-beans-burger.jpg', '', 'Method:\r\nHeat grill to high. Tip the beans into a large bowl, then roughly crush with a potato masher. Add the breadcrumbs, chilli powder, coriander stalks and ½ the leaves, egg and 2 tbsp salsa, season to taste, then mix together well with a fork.\r\nDivide the mixture into 6, then wet your hands and shape into burgers. The burgers can now be frozen. Place on a non-stick baking tray, then grill for 4-5 mins on each side until golden and crisp. To cook from frozen, bake at 200C/fan 180C/gas 6 for 20-30 mins until hot through.\r\nWhile the burgers are cooking, mix the remaining coriander leaves with the yogurt, lime juice and a good grind of black pepper. Split the buns in half and spread the bases with some of the yogurt. Top each with leaves, avocado, onion, a burger, another dollop of the lime yogurt and some salsa, then serve.', 195, NULL),
(38, 4, 'Coconut & squash dhansak', 'resources/images/recipes/coconut-squash-dhansak.jpg', 'Ingredients: \r\n1 tbsp vegetable oil, \r\n500g butternut squash (about 1 small squash), peeled and chopped into bite-sized chunks, \r\n100g frozen chopped onions, \r\n4 heaped tbsp mild curry paste, \r\nchopped tomatoes, \r\n400g can light coconut milk, \r\nmini naan bread, to serve, \r\n400g can lentils, drained, \r\n200g bag baby spinach, \r\n150ml coconut yogurt, plus extra to serve. ', 'Method:\r\nHeat the oil in a large pan. Put the squash in a bowl with a splash of water. Cover with cling film and microwave on High for 10 mins or until tender. Meanwhile, add the onions to the hot oil and cook for a few mins until soft. Add the curry paste, tomatoes and coconut milk, and simmer for 10 mins until thickened to a rich sauce.\r\nWarm the naan breads in a low oven or in the toaster. Drain any liquid from the squash, then add to the sauce with the lentils, spinach and some seasoning. Simmer for a further 2-3 mins to wilt the spinach, then stir in the coconut yogurt. Serve with the warm naan and a dollop of extra yogurt.', 320, NULL),
(39, 4, 'Smoky aubergine & pepper tagine', 'resources/images/recipes/Smoky-aubergine-and-pepper-tagine.jpg', 'Ingredients: \r\n2 aubergines, cut roughly into small chunks, \r\n3 mixed peppers (not green) cut into small chunks, \r\n1 red onion, roughly chopped, \r\n2 tbsp olive oil, \r\n3 tbsp baharat spice mix, \r\n2 garlic cloves, crushed or finely grated, \r\n2 x 400g cans chopped tomatoes, \r\n200ml vegetable stock or water, \r\n700g jar haricot beans, drained and rinsed.\r\nTo serve: \r\nsmoked or regular sea salt flakes, \r\ndrizzle of extra virgin olive oil, \r\nchopped mint and/or coriander, \r\nhandful of toasted flaked almonds, \r\ncouscous or flatbreads.', 'Method:\r\nHeat the oven to 220C/200C fan/gas 7. Mix the aubergines, peppers, onions, olive oil, baharat, garlic and 1 tsp sea salt flakes in a large, deep roasting tin. Roast for 30-40 mins until the vegetables are just charring around the edges.\r\nAdd the tomatoes, stock or water and haricot beans, mix well, then return to the oven for a further 30 mins to reduce the sauce. Will keep frozen for up to three months. Leave to cool completely first. Defrost in the fridge overnight, then warm through in a pan or roasting tin in the oven at 180C/160C fan/gas 4 for 30 mins or until piping hot.\r\nTaste and adjust the salt as needed using the smoked sea salt, if you have it. Drizzle with the extra virgin olive oil, then scatter with the herbs and flaked almonds before serving hot, with couscous or flatbreads on the side.', 283, NULL),
(40, 4, 'Winter vegetable pie', 'resources/images/recipes/winter-vegetable-pie.jpg', 'Ingredients:\r\n2 tbsp olive oil, \r\n2 onions, sliced, \r\n1 tbsp flour, \r\n300g (about 2 large) carrot, cut into small batons, \r\n½ cauliflower, broken into small florets, \r\n4 garlic cloves, finely sliced, \r\n1 rosemary sprig, leaves finely chopped, \r\n400g can chopped tomato, \r\n200g frozen pea, \r\n900g potato, cut into chunks, \r\nup to 200ml/7fl oz milk. ', 'Method: \r\nHeat 1 tbsp of the oil in a flameproof dish over a medium heat. Add the onions and cook for 10 mins until softened, then stir in the flour and cook for a further 2 mins. Add the carrots, cauliflower, garlic and rosemary, and cook for 5 mins, stirring regularly, until they begin to soften.\r\nTip the tomatoes into the vegetables along with a can full of water. Cover with a lid and simmer for 10 mins, then remove the lid and cook for 10-15 mins more, until the sauce has thickened and the vegetables are cooked. Season, stir in the peas and cook for 1 min more.\r\nMeanwhile, boil the potatoes for 10-15 mins until tender. Drain, then place back in the saucepan and mash. Stir through enough milk to reach a fairly soft consistency, then add the remaining olive oil and season.\r\nHeat the grill. Spoon the hot vegetable mix into a pie dish, top with the mash and drag a fork lightly over the surface. Place under the grill for a few mins until the top is crisp golden brown.', 388, NULL);

-- --------------------------------------------------------

--
-- Struttura della tabella `userexercises`
--

CREATE TABLE `userexercises` (
  `user_id` bigint UNSIGNED NOT NULL,
  `exercise_id` int NOT NULL,
  `day` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dump dei dati per la tabella `userexercises`
--

INSERT INTO `userexercises` (`user_id`, `exercise_id`, `day`) VALUES
(4, 1, '2023-08-22 11:32:45'),
(4, 1, '2023-08-29 22:51:46'),
(4, 1, '2023-08-29 22:52:16'),
(4, 1, '2023-09-05 13:01:14'),
(4, 1, '2023-09-07 15:41:05'),
(4, 1, '2023-09-09 11:32:50'),
(4, 2, '2023-08-22 11:38:08'),
(4, 2, '2023-08-22 19:46:35'),
(4, 2, '2023-08-29 22:51:46'),
(4, 2, '2023-08-29 22:52:16'),
(4, 2, '2023-09-05 20:11:39'),
(4, 2, '2023-09-17 17:46:58'),
(4, 3, '2023-09-05 20:11:39'),
(4, 3, '2023-09-17 17:46:58'),
(4, 4, '2023-09-08 07:43:04'),
(4, 4, '2023-09-17 17:46:58'),
(4, 4, '2023-09-22 23:12:20'),
(4, 5, '2023-09-08 07:43:04'),
(4, 5, '2023-09-22 23:12:20'),
(4, 6, '2023-09-05 14:13:47'),
(4, 6, '2023-09-08 07:43:25'),
(4, 6, '2023-09-17 23:37:53'),
(4, 7, '2023-09-07 13:51:05'),
(4, 7, '2023-09-08 07:43:26'),
(4, 8, '2023-09-08 07:43:26'),
(4, 8, '2023-09-14 13:32:39'),
(4, 13, '2023-08-29 22:51:46'),
(4, 13, '2023-08-29 22:52:16'),
(4, 13, '2023-08-30 09:13:35'),
(4, 13, '2023-08-30 11:29:48'),
(4, 13, '2023-08-30 11:30:05'),
(4, 13, '2023-09-05 20:11:39'),
(4, 14, '2023-08-29 22:51:46'),
(4, 14, '2023-08-29 22:52:16'),
(4, 14, '2023-08-30 09:11:17'),
(4, 14, '2023-08-30 09:13:35'),
(4, 14, '2023-08-30 11:30:05'),
(4, 14, '2023-09-17 17:46:58'),
(4, 14, '2023-09-22 23:06:18'),
(4, 14, '2023-09-22 23:06:44'),
(4, 15, '2023-08-29 22:52:40'),
(4, 16, '2023-08-29 22:52:40'),
(4, 16, '2023-08-29 22:52:57'),
(4, 19, '2023-08-29 22:55:18'),
(4, 20, '2023-09-07 15:25:13'),
(4, 20, '2023-09-08 07:43:43'),
(4, 21, '2023-09-08 07:43:43'),
(4, 32, '2023-09-23 16:22:55');

-- --------------------------------------------------------

--
-- Struttura della tabella `usermeals`
--

CREATE TABLE `usermeals` (
  `user_id` bigint UNSIGNED NOT NULL,
  `recipe_id` int NOT NULL,
  `day` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dump dei dati per la tabella `usermeals`
--

INSERT INTO `usermeals` (`user_id`, `recipe_id`, `day`) VALUES
(4, 1, '2023-08-24 10:10:27'),
(4, 1, '2023-08-25 15:03:11'),
(4, 1, '2023-08-26 21:37:50'),
(4, 1, '2023-08-28 17:16:22'),
(4, 1, '2023-08-28 17:16:41'),
(4, 1, '2023-09-08 12:19:24'),
(4, 1, '2023-09-09 11:33:09'),
(4, 1, '2023-09-24 21:43:06'),
(6, 1, '2023-09-03 21:42:27'),
(4, 2, '2023-08-29 22:13:12'),
(4, 2, '2023-09-05 14:03:49'),
(4, 2, '2023-09-17 23:38:15'),
(4, 13, '2023-08-28 15:43:45'),
(4, 13, '2023-09-17 17:46:10'),
(4, 14, '2023-08-28 15:42:30'),
(4, 14, '2023-08-28 15:43:45'),
(4, 14, '2023-08-29 22:45:16'),
(4, 14, '2023-09-08 07:40:21'),
(4, 15, '2023-08-24 09:36:13'),
(4, 15, '2023-08-24 10:10:27'),
(4, 15, '2023-08-28 15:43:45'),
(4, 15, '2023-08-29 22:45:07'),
(4, 15, '2023-08-29 22:45:16'),
(4, 15, '2023-08-29 22:45:37'),
(4, 15, '2023-09-08 21:46:53'),
(2, 16, '2023-09-05 13:54:39'),
(4, 16, '2023-08-24 09:55:05'),
(4, 16, '2023-08-25 15:22:46'),
(4, 16, '2023-08-29 13:28:26'),
(4, 16, '2023-09-05 14:09:50'),
(4, 16, '2023-09-07 13:51:36'),
(6, 16, '2023-09-03 21:42:06'),
(4, 17, '2023-08-25 15:22:46'),
(4, 18, '2023-08-25 15:22:46'),
(4, 19, '2023-08-25 15:22:46'),
(4, 19, '2023-09-28 09:21:58'),
(4, 20, '2023-08-25 15:22:46'),
(4, 20, '2023-09-08 07:41:29'),
(4, 21, '2023-08-25 15:22:21'),
(4, 21, '2023-08-29 13:28:52'),
(4, 21, '2023-09-05 13:42:47'),
(4, 21, '2023-09-07 13:52:24'),
(4, 21, '2023-09-08 07:41:43'),
(4, 22, '2023-08-25 15:22:21'),
(4, 22, '2023-08-29 13:28:52'),
(4, 22, '2023-09-17 17:46:41'),
(4, 23, '2023-08-25 15:11:41'),
(4, 23, '2023-08-25 15:12:00'),
(4, 23, '2023-08-25 15:12:04'),
(4, 23, '2023-08-29 13:30:20'),
(4, 24, '2023-08-25 15:11:41'),
(4, 24, '2023-08-25 15:12:00'),
(4, 24, '2023-08-25 15:12:04'),
(4, 24, '2023-08-28 17:26:39'),
(4, 24, '2023-08-29 13:30:20'),
(4, 24, '2023-09-08 07:41:55'),
(4, 29, '2023-09-08 21:46:53'),
(4, 30, '2023-09-22 23:07:09'),
(4, 31, '2023-09-17 17:46:41'),
(4, 33, '2023-09-22 23:14:33'),
(4, 35, '2023-09-17 17:46:41'),
(4, 35, '2023-09-22 23:14:33'),
(4, 37, '2023-09-17 17:46:24');

-- --------------------------------------------------------

--
-- Struttura della tabella `users`
--

CREATE TABLE `users` (
  `id` bigint UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `age` int NOT NULL,
  `gender` varchar(128) COLLATE utf8mb4_unicode_ci NOT NULL,
  `admin` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dump dei dati per la tabella `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`, `age`, `gender`, `admin`) VALUES
(1, 'mirko', 'mirko@gmx.at', NULL, '$2y$10$WzQJfVCUJzLm9/wr3w.AAeRpyD0F0fRo1iOFwG7Oi6EyoufAgK6JW', NULL, '2023-08-04 09:17:16', '2023-08-04 09:17:16', 25, '', 0),
(2, 'user1', 'user@gmail.com', NULL, '$2y$10$6NnCTS7KlBdCfQtRi.41F.UdIHrzzPTA8a9HSz4KBHh8Ijsmga8vm', NULL, '2023-08-22 07:08:36', '2023-08-22 07:08:36', 26, 'male', 0),
(3, 'User2', 'user2@gmail.com', NULL, '$2y$10$bRSSDMvzdxsR.SUUmUQLQue/YIRcnKXr0vQwzy8nkQPYSIUWwufHe', NULL, '2023-08-24 07:09:34', '2023-08-24 07:09:34', 27, 'male', 0),
(4, 'Mirko', 'mirko@domain.com', NULL, '$2y$10$o9/3Gkfcpp.ggTsAeKVJs.MBxfxTO74/LqchiymE8j0FKmYnSRutm', NULL, '2023-09-03 19:41:25', '2023-09-03 19:41:25', 28, 'male', 1),
(7, 'user3', 'user3@domain.com', NULL, '$2y$10$lh2jbap4W2f/73ouFArnVOYG68xVqbiZe5BEG.0ROFAYpRDxnMku.', NULL, '2023-09-20 15:47:44', '2023-09-20 15:47:44', 22, 'male', 0);

--
-- Indici per le tabelle scaricate
--

--
-- Indici per le tabelle `Coaches`
--
ALTER TABLE `Coaches`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email_address` (`email_address`);

--
-- Indici per le tabelle `coach_reviews`
--
ALTER TABLE `coach_reviews`
  ADD PRIMARY KEY (`id`),
  ADD KEY `coach_reviews_fk` (`coach_id`);

--
-- Indici per le tabelle `dailycalories`
--
ALTER TABLE `dailycalories`
  ADD PRIMARY KEY (`user_id`,`day`);

--
-- Indici per le tabelle `exercisecategories`
--
ALTER TABLE `exercisecategories`
  ADD PRIMARY KEY (`category_id`);

--
-- Indici per le tabelle `exercises`
--
ALTER TABLE `exercises`
  ADD PRIMARY KEY (`exercise_id`),
  ADD KEY `exercises_ibfk_1` (`category_id`);

--
-- Indici per le tabelle `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`);

--
-- Indici per le tabelle `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indici per le tabelle `password_reset_tokens`
--
ALTER TABLE `password_reset_tokens`
  ADD PRIMARY KEY (`email`);

--
-- Indici per le tabelle `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `personal_access_tokens_token_unique` (`token`),
  ADD KEY `personal_access_tokens_tokenable_type_tokenable_id_index` (`tokenable_type`,`tokenable_id`);

--
-- Indici per le tabelle `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- Indici per le tabelle `recipecategories`
--
ALTER TABLE `recipecategories`
  ADD PRIMARY KEY (`category_id`);

--
-- Indici per le tabelle `recipes`
--
ALTER TABLE `recipes`
  ADD PRIMARY KEY (`recipe_id`),
  ADD KEY `recipes_ibfk_1` (`category_id`);

--
-- Indici per le tabelle `userexercises`
--
ALTER TABLE `userexercises`
  ADD PRIMARY KEY (`user_id`,`exercise_id`,`day`),
  ADD KEY `exercise_id` (`exercise_id`);

--
-- Indici per le tabelle `usermeals`
--
ALTER TABLE `usermeals`
  ADD PRIMARY KEY (`user_id`,`recipe_id`,`day`),
  ADD KEY `recipe_id` (`recipe_id`);

--
-- Indici per le tabelle `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT per le tabelle scaricate
--

--
-- AUTO_INCREMENT per la tabella `Coaches`
--
ALTER TABLE `Coaches`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT per la tabella `coach_reviews`
--
ALTER TABLE `coach_reviews`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT per la tabella `exercisecategories`
--
ALTER TABLE `exercisecategories`
  MODIFY `category_id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT per la tabella `exercises`
--
ALTER TABLE `exercises`
  MODIFY `exercise_id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;

--
-- AUTO_INCREMENT per la tabella `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT per la tabella `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT per la tabella `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT per la tabella `products`
--
ALTER TABLE `products`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT per la tabella `recipecategories`
--
ALTER TABLE `recipecategories`
  MODIFY `category_id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT per la tabella `recipes`
--
ALTER TABLE `recipes`
  MODIFY `recipe_id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=56;

--
-- AUTO_INCREMENT per la tabella `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- Limiti per le tabelle scaricate
--

--
-- Limiti per la tabella `coach_reviews`
--
ALTER TABLE `coach_reviews`
  ADD CONSTRAINT `coach_reviews_fk` FOREIGN KEY (`coach_id`) REFERENCES `Coaches` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;

--
-- Limiti per la tabella `dailycalories`
--
ALTER TABLE `dailycalories`
  ADD CONSTRAINT `dailycalories_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Limiti per la tabella `exercises`
--
ALTER TABLE `exercises`
  ADD CONSTRAINT `exercises_ibfk_1` FOREIGN KEY (`category_id`) REFERENCES `exercisecategories` (`category_id`);

--
-- Limiti per la tabella `recipes`
--
ALTER TABLE `recipes`
  ADD CONSTRAINT `recipes_ibfk_1` FOREIGN KEY (`category_id`) REFERENCES `recipecategories` (`category_id`);

--
-- Limiti per la tabella `userexercises`
--
ALTER TABLE `userexercises`
  ADD CONSTRAINT `userexercises_fk` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;

--
-- Limiti per la tabella `usermeals`
--
ALTER TABLE `usermeals`
  ADD CONSTRAINT `recipe_id` FOREIGN KEY (`recipe_id`) REFERENCES `recipes` (`recipe_id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
