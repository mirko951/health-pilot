@extends('admin.layouts')

@section('content')
<div class="container" style="margin-top: 100px !important;">
    @if(session('success'))
        <div class="alert alert-success">
            {{ session('success') }}
        </div>
    @endif

    @if(session('error'))
        <div class="alert alert-danger">
            {{ session('error') }}
        </div>
    @endif
    
    @if(isset($deletedExercise))
        <div class="alert alert-info">
            Exercise Deleted: {{ $deletedExercise->name }}
        </div>
    @endif

    <div class="d-flex justify-content-between align-items-center mb-4">
        <h2>Exercise List</h2>
        <form method="get" action="{{ route('create-exercise') }}">
            <button type="submit" class="btn btn-primary">Create Exercise</button>
        </form>
    </div>

    @php
        $softDeletedExercises = $exercises->filter(function($exercise) {
            return $exercise->trashed();
        });
    @endphp
   
    @if ($softDeletedExercises->count() > 0)
    <form method="post" action="{{ route('restore-all-exercises') }}">
        @csrf
        @method('PUT') <!-- Use the PUT method to restore all exercises -->
        <button type="submit" class="btn btn-success">Restore All</button>
    </form>
    @endif

    <table class="table table-striped">
        <thead>
            <tr>
                <th>Name</th>
                <th>Photo</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($exercises as $exercise)
                @if (!$exercise->trashed())
                    <tr>
                        <td>{{ $exercise->name }}</td>
                        <td>
                            @if ($exercise->image_url)
                                <img src="{{ asset($exercise->image_url) }}" alt="Exercise Preview" width="100">
                            @else
                                <span>No Image</span>
                            @endif
                        </td>
                        <td>
                            <div class="btn-group" role="group">
                                <a href="{{ route('edit-exercise', ['exercise_id' => $exercise->exercise_id]) }}" class="btn btn-primary">Edit</a>
                                <form method="post" action="{{ route('delete-exercise', ['exercise_id' => $exercise->exercise_id]) }}">
                                    @csrf
                                    @method('DELETE')
                                    <button type="submit" class="btn btn-danger">Delete</button>
                                </form>
                            </div>
                        </td>
                    </tr>
                @endif
            @endforeach
        </tbody>
    </table>
</div>
@endsection
