@extends('admin.layouts')

@section('content')
<div class="container" style="margin-top: 100px !important;">
    <h2>Create New Product</h2>

    @if(session('success'))
        <div class="alert alert-success">
            {{ session('success') }}
        </div>
    @endif

    @if(session('error'))
        <div class="alert alert-danger">
            {{ session('error') }}
        </div>
    @endif

    <form method="post" action="{{ route('admin.store-product') }}" enctype="multipart/form-data">
        @csrf

        <div class="form-group">
            <label for="product_name">Product Name</label>
            <input type="text" class="form-control" id="product_name" name="product_name" required>
        </div>

        <div class="form-group">
            <label for="photo">Product Photo</label>
            <input type="file" class="form-control-file" id="photo" name="photo">
        </div>

        <div class="form-group">
            <label for="product_description">Product Description</label>
            <textarea class="form-control" id="product_description" name="product_description"></textarea>
        </div>

        <div class="form-group">
            <label for="price">Price</label>
            <input type="text" class="form-control" id="price" name="price" required>
        </div>

        <!-- Add other product fields here -->

        <button type="submit" class="btn btn-primary">Create Product</button>
    </form>
</div>
@endsection
