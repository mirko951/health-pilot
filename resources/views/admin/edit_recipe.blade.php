@extends('admin.layouts')

@section('content')
<div class="container" style="margin-top: 100px;">
    <h2>Edit recipe</h2>

    @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif
    <form method="post" action="{{ route('update-recipe', ['recipe_id' => $recipe->recipe_id]) }}">
        @csrf
        @method('PUT')

        <div class="form-group">
            <label for="name">Name</label>
            <input type="text" class="form-control" id="name" name="name" value="{{ $recipe->name }}">
        </div>

        <!-- <div class="form-group">
            <label for="image_url">Image URL</label>
            <input type="text" class="form-control" id="image_url" name="image_url" value="{{ $recipe->image_url }}">
        </div> -->

        <div class="form-group">
            <label for="ingredients">Ingredients</label>
            <textarea class="form-control" id="ingredients" name="ingredients">{{ $recipe->ingredients }}</textarea>
        </div>

        <div class="form-group">
            <label for="method">Method</label>
            <input type="text" class="form-control" id="method" name="method" value="{{ $recipe->method }}">
        </div>

        <div class="form-group">
            <label for="calories_per_serving">Calories</label>
            <input type="text" class="form-control" id="calories_per_serving" name="calories_per_serving" value="{{ $recipe->calories_per_serving }}">
        </div>

        <button type="submit" class="btn btn-primary" style="margin-top: 10px;">Save Changes</button>
    </form>
</div>
@endsection
