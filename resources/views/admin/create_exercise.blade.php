@extends('admin.layouts')

@section('content')
<div class="container" style="margin-top: 100px;">
    @if(session('success'))
        <div class="alert alert-success">
            {{ session('success') }}
        </div>
    @endif

    @if(session('error'))
        <div class="alert alert-danger">
            {{ session('error') }}
        </div>
    @endif

    <h2>Create a New Exercise</h2>
    @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif
    
    <form method="post" action="{{ route('store-exercise') }}" enctype="multipart/form-data">
        @csrf

        <div class="form-group">
            <label for="name">Exercise Name:</label>
            <input type="text" id="name" name="name" class="form-control" required>
        </div>

        <div class="form-group">
            <label for="photo">Exercise Photo:</label>
            <input type="file" id="image_url" name="image_url" class="form-control-file">
        </div>

        <div class="form-group">
            <label for="description">Exercise Description:</label>
            <textarea id="description" name="description" class="form-control"></textarea>
        </div>

        <div class="form-group">
            <label for="category_id">Exercise Category:</label>
            <select id="category_id" name="category_id" class="form-control" required>
                <option value="1">Abdominals</option>
                <option value="2">Arms</option>
                <option value="3">Back/Schoulders</option>
                <option value="4">Legs</option>
            </select>
        </div>

        <div class="form-group">
            <label for="rep">Repetition:</label>
            <input type="text" id="rep" name="rep" class="form-control" required>
        </div>

        <div class="form-group">
            <label for="calories">Calories:</label>
            <input type="text" id="calories" name="calories" class="form-control" required>
        </div>

        <button type="submit" class="btn btn-primary">Create exercise</button>
    </form>
</div>
@endsection
