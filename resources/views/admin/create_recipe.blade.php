@extends('admin.layouts')

@section('content')
<div class="container" style="margin-top: 100px;">
    @if(session('success'))
        <div class="alert alert-success">
            {{ session('success') }}
        </div>
    @endif

    @if(session('error'))
        <div class="alert alert-danger">
            {{ session('error') }}
        </div>
    @endif

    <h2>Create a New Recipe</h2>
    
    <form method="post" action="{{ route('store-recipe') }}" enctype="multipart/form-data">
        @csrf

        <div class="form-group">
            <label for="name">Recipe Name:</label>
            <input type="text" id="name" name="name" class="form-control" required>
        </div>

        <div class="form-group">
            <label for="photo">Recipe Photo:</label>
            <input type="file" id="image_url" name="image_url" class="form-control-file">
        </div>

        <div class="form-group">
            <label for="method">Recipe Method:</label>
            <textarea id="method" name="method" class="form-control"></textarea>
        </div>

        <div class="form-group">
            <label for="category_id">Recipe Category:</label>
            <select id="category_id" name="category_id" class="form-control" required>
                <option value="1">Breakfast</option>
                <option value="2">Snack</option>
                <option value="3">Lunch</option>
                <option value="4">Dinner</option>
            </select>
        </div>

        <div class="form-group">
            <label for="ingredients">Ingredients:</label>
            <input type="text" id="ingredients" name="ingredients" class="form-control" required>
        </div>

        <div class="form-group">
            <label for="calories_per_serving">Calories:</label>
            <input type="text" id="calories_per_serving" name="calories_per_serving" class="form-control" required>
        </div>


        <button type="submit" class="btn btn-primary">Create Recipe</button>
    </form>
</div>
@endsection
