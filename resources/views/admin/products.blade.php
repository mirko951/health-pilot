@extends('admin.layouts')

@section('content')
<div class="container" style="margin-top: 100px;">
    @if(session('success'))
        <div class="alert alert-success">
            {{ session('success') }}
        </div>
    @endif

    @if(session('error'))
        <div class="alert alert-danger">
            {{ session('error') }}
        </div>
    @endif
    
    @if(isset($deletedProduct))
        <div class="alert alert-info">
            Product Deleted: {{ $deletedProduct->product_name }}
        </div>
    @endif

    <div class="d-flex justify-content-between align-items-center mb-4">
        <h2>Product List</h2>
        <form method="get" action="{{ route('admin.create-product') }}">
            @csrf
            <button type="submit" class="btn btn-primary">Create Product</button>
        </form>
    </div>

    @php
        $softDeletedProducts = $products->filter(function($product) {
            return $product->trashed();
        });
    @endphp

    @if ($softDeletedProducts->count() > 0)
        <form method="post" action="{{ route('admin.restore-all-products') }}">
            @csrf
            @method('PUT') <!-- Use the PUT method to restore all products -->
            <button type="submit" class="btn btn-success">Restore All</button>
        </form>
    @endif

    <table class="table table-striped">
        <thead>
            <tr>
                <th>Name</th>
                <th>Photo</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($products as $product)
                @if (!$product->trashed())
                    <tr>
                        <td>{{ $product->product_name }}</td>
                        <td>
                            @if ($product->photo)
                                <img src="{{ asset($product->photo) }}" alt="Product Preview" width="100">
                            @else
                                <span>No Image</span>
                            @endif
                        </td>
                        <td>
                            <div class="btn-group" role="group">
                            <a href="{{ route('admin.edit-product', ['id' => $product->id]) }}" class="btn btn-primary">Edit</a>
                                <form method="post" action="{{ route('admin.delete-product', ['id' => $product->id]) }}">
                                    @csrf
                                    @method('DELETE')
                                    <button type="submit" class="btn btn-danger">Delete</button>
                                </form>
                            </div>
                        </td>
                    </tr>
                @endif
            @endforeach
        </tbody>
    </table>
</div>
@endsection
