@extends('admin.layouts')

@section('content')
<div class="container" style="margin-top: 100px !important;">
    @if(session('success'))
        <div class="alert alert-success">
            {{ session('success') }}
        </div>
    @endif

    @if(session('error'))
        <div class="alert alert-danger">
            {{ session('error') }}
        </div>
    @endif
    
    @if(isset($deletedCoach))
        <div class="alert alert-info">
            Coach Deleted: {{ $deletedCoach->name }}
        </div>
    @endif

    <div class="d-flex justify-content-between align-items-center mb-4">
        <h2>Coach List</h2>
        <form method="get" action="{{ route('admin.create-coach') }}">
            <button type="submit" class="btn btn-primary">Create Coach</button>
        </form>
    </div>

   


    @if ($SoftDeletedCoaches->count() > 0)
    <form method="post" action="{{ route('admin.restore-all-coaches') }}">
        @csrf
        @method('PUT') <!-- Use the PUT method to restore all coachs -->
        <button type="submit" class="btn btn-success">Restore All</button>
    </form>
    @endif

    <table class="table table-striped">
        <thead>
            <tr>
                <th>Name</th>
                <th>Photo</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($coaches as $coach)
                @if (!$coach->trashed())
                    <tr>
                        <td>{{ $coach->name }}</td>
                        <td>
                            @if ($coach->photo)
                                <img src="{{ asset($coach->photo) }}" alt="Coach Preview" width="100">
                            @else
                                <span>No Image</span>
                            @endif
                        </td>
                        <td>
                            <div class="btn-group" role="group">
                                <a href="{{ route('admin.edit-coach', ['id' => $coach->id]) }}" class="btn btn-primary">Edit</a>
                                <form method="post" action="{{ route('admin.delete-coach', ['id' => $coach->id]) }}">
                                    @csrf
                                    @method('DELETE')
                                    <button type="submit" class="btn btn-danger">Delete</button>
                                </form>
                            </div>
                        </td>
                    </tr>
                @endif
            @endforeach
        </tbody>
    </table>
</div>
@endsection
