@extends('admin.layouts')

@section('content')
<div class="container" style="margin-top: 100px;">
    @if(session('error'))
        <div class="alert alert-danger">
            {{ session('error') }}
        </div>
    @endif

    <h2>Edit Coach</h2>

    <form method="post" action="{{ route('admin.update-coach', ['id' => $coach->id]) }}" enctype="multipart/form-data">
        @csrf
        @method('PUT')

        <div class="form-group">
            <label for="name">Coach Name:</label>
            <input type="text" id="name" name="name" class="form-control" value="{{ $coach->name }}" required>
        </div>

        <div class="form-group">
            <label for="photo">Coach Photo:</label>
            <input type="file" id="photo" name="photo" class="form-control-file">
        </div>

        <div class="form-group">
            <label for="description">Coach Description:</label>
            <textarea id="description" name="description" class="form-control">{{ $coach->description }}</textarea>
        </div>

        <div class="form-group">
            <label for="email_address">Email Address:</label>
            <input type="email" id="email_address" name="email_address" class="form-control" value="{{ $coach->email_address }}">
        </div>

        <button type="submit" class="btn btn-primary">Save Changes</button>
    </form>
</div>
@endsection
