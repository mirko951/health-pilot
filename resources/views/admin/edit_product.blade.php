@extends('admin.layouts')

@section('content')
<div class="container" style="margin-top: 100px !important;">
    <h2>Edit Product</h2>

    <form method="post" action="{{ route('admin.update-product', ['id' => $product->id]) }}" enctype="multipart/form-data">
        @csrf
        @method('PUT')

        <div class="form-group">
            <label for="product_name">Product Name</label>
            <input type="text" class="form-control" id="product_name" name="product_name" value="{{ $product->product_name }}" required>
        </div>

        <!-- Display the current photo for reference -->
        <div class="form-group">
            <label for="current_photo">Current Photo</label>
            <img src="{{ asset($product->photo) }}" alt="Current Photo" width="100">
        </div>

        <div class="form-group">
            <label for="product_description">Product Description</label>
            <textarea class="form-control" id="product_description" name="product_description">{{ $product->product_description }}</textarea>
        </div>

        <div class="form-group">
            <label for="price">Price</label>
            <input type="text" class="form-control" id="price" name="price" value="{{ $product->price }}" required>
        </div>

        <button type="submit" class="btn btn-primary">Save Changes</button>
    </form>
</div>
@endsection
