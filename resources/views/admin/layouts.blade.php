<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Admin Dashboard</title>
    
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-4bw+/aepP/YC94hEpVNVgiZdgIC5+VKNBQNGCHeKRQN+PtmoHDEXuppvnDJzQIu9" crossorigin="anonymous">
    
  
</head>
<body>
    <nav class="navbar">
        <div class="container">
            <a href="{{ route('admin.exercises') }}" class="navbar-brand">Admin Dashboard</a>
            <ul class="nav">
                <li class="nav-item">
                    <a href="{{ route('admin.exercises') }}" class="nav-link">Exercises</a>
                </li>
                <li class="nav-item">
                    <a href="{{ route('admin.recipes') }}" class="nav-link">Recipes</a>
                </li>
                <li class="nav-item">
                    <a href="{{ route('admin.products') }}" class="nav-link">Products</a>
                </li>
                <li class="nav-item">
                    <a href="{{ route('admin.coaches') }}" class="nav-link">Coaches</a>
                </li>
                <li class="nav-item">
                    <a href="{{ route('dashboard') }}" class="nav-link">Back to the Website</a>
                </li>
            </ul>
        </div>
    </nav>

    <main class="main-content">
        <div class="container">
            @yield('content')
        </div>
    </main>

    <footer class="py-3 mt-4 text-center ">
            <div class="container">
                <p>Health Pilot <img src="{{asset('resources/images/icons/icons8-copyright-50.png')}}" alt="copyright" height="20px">2023 -- 1234, Vienna +43 123456789 email: HealthPilot@domain.at
                </p>
            </div>
        </footer>
</body>
</html>
