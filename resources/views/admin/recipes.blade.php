@extends('admin.layouts')

@section('content')
<div class="container" style="margin-top: 100px !important;">
    @if(session('success'))
        <div class="alert alert-success">
            {{ session('success') }}
        </div>
    @endif

    @if(session('error'))
        <div class="alert alert-danger">
            {{ session('error') }}
        </div>
    @endif
    
    @if(isset($deletedrecipe))
        <div class="alert alert-info">
            Recipe Deleted: {{ $deletedrecipe->name }}
        </div>
    @endif

    <div class="d-flex justify-content-between align-items-center mb-4">
        <h2>Recipe List</h2>
        <form method="get" action="{{ route('create-recipe') }}">
            <button type="submit" class="btn btn-primary">Create Recipe</button>
        </form>
    </div>

    @php
        $softDeletedrecipes = $recipes->filter(function($recipe) {
            return $recipe->trashed();
        });
    @endphp

    @if ($softDeletedrecipes->count() > 0)
    <form method="post" action="{{ route('restore-all-recipes') }}">
        @csrf
        @method('PUT') <!-- Use the PUT method to restore all recipes -->
        <button type="submit" class="btn btn-success">Restore All</button>
    </form>
    @endif

    <table class="table table-striped">
        <thead>
            <tr>
                <th>Name</th>
                <th>Photo</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($recipes as $recipe)
                @if (!$recipe->trashed())
                    <tr>
                        <td>{{ $recipe->name }}</td>
                        <td>
                            @if ($recipe->image_url)
                                <img src="{{ asset($recipe->image_url) }}" alt="Recipe Preview" width="100">
                            @else
                                <span>No Image</span>
                            @endif
                        </td>
                        <td>
                            <div class="btn-group" role="group">
                                <a href="{{ route('edit-recipe', ['recipe_id' => $recipe->recipe_id]) }}" class="btn btn-primary">Edit</a>
                                <form method="post" action="{{ route('delete-recipe', ['recipe_id' => $recipe->recipe_id]) }}">
                                    @csrf
                                    @method('DELETE')
                                    <button type="submit" class="btn btn-danger">Delete</button>
                                </form>
                            </div>
                        </td>
                    </tr>
                @endif
            @endforeach
        </tbody>
    </table>
</div>
@endsection
