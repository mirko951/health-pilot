@extends('admin.layouts')

@section('content')
<div class="container" style="margin-top: 100px;">
    <h2>Edit Exercise</h2>

    <form method="post" action="{{ route('update-exercise', ['exercise_id' => $exercise->exercise_id]) }}">
        @csrf
        @method('PUT') {{-- Use the PUT method for updates --}}

        <div class="form-group">
            <label for="name">Name</label>
            <input type="text" class="form-control" id="name" name="name" value="{{ $exercise->name }}">
        </div>

        <div class="form-group">
            <label for="image_url">Image URL</label>
            <input type="text" class="form-control" id="image_url" name="image_url" value="{{ $exercise->image_url }}">
        </div>

        <div class="form-group">
            <label for="description">Description</label>
            <textarea class="form-control" id="description" name="description">{{ $exercise->description }}</textarea>
        </div>

        <div class="form-group">
            <label for="rep">Repetition</label>
            <input type="text" class="form-control" id="rep" name="rep" value="{{ $exercise->rep }}">
        </div>

        <div class="form-group">
            <label for="calories">Calories</label>
            <input type="text" class="form-control" id="calories" name="calories" value="{{ $exercise->calories }}">
        </div>

        <button type="submit" class="btn btn-primary" style="margin-top: 10px;">Save Changes</button>
    </form>
</div>
@endsection
