@extends('auth.layouts')

@section('content')
<div class="container-fluid">
  <div class="row">
    <div class="col-md-12 title-bg myProgress-div">
      <div class="position-absolute top-0 start-0 h-100 w-100 d-flex align-items-center justify-content-start myProgress">
        <p class="myProgress-text orange-text">MY PROGRESS</p>
      </div>
    </div>
  </div>
</div>

<div class="container">
  <h2 class="mt-4 mb-3" style="color: white;">View Monthly Data</h2>

  <form method="get" action="{{ route('myprogress') }}">
    @csrf
    <div class="mb-3">
      <label for="selected_month" class="form-label" style="color: white;">Select A Month:</label>
      <input type="month" id="selected_month" name="selected_month" class="form-control" required
             value="{{ old('selected_month', request('selected_month', Carbon\Carbon::now()->format('Y-m'))) }}">
    </div>
    <button type="submit" class="btn btn-primary">View</button>
  </form>

  @if(isset($meals_kcal_total) && isset($exercises_kcal_total))
    <div class="row mt-5">
      <div class="col-md-12">
        <h3 style="color: white;">Monthly Summary</h3>
        <table class="table table-bordered">
          <thead>
            <tr>
              <th style="background-color: orange;">Day</th>
              <th style="background-color: orange;">Food (kcal)</th>
              <th style="background-color: orange;">Workout (kcal)</th>
              <th style="background-color: orange;">Total (kcal)</th>
            </tr>
          </thead>
          <tbody>
            @php
              // Ordina i giorni in modo crescente
              ksort($meals_kcal_total);
            @endphp
            @foreach($meals_kcal_total as $day => $meals_kcal)
              <tr>
                <td style="background-color: rgba(237, 138, 24, 0.90); color: white;">{{ $day }}</td>
                <td style="background-color: rgba(237, 138, 24, 0.90); color: white;">{{ $meals_kcal }}</td>
                <td style="background-color: rgba(237, 138, 24, 0.90); color: white;">{{ $exercises_kcal_total[$day] ?? 0 }}</td>
                <td style="background-color: rgba(237, 138, 24, 0.90); color: white;">{{ $meals_kcal - ($exercises_kcal_total[$day] ?? 0) }}</td>
              </tr>
            @endforeach
          </tbody>
        </table>
      </div>
    </div>
  @endif

  <div>
    <h1 class="orange-text" style="font-size: 400%; text-align: center; margin-bottom: 70px; margin-top:70px;">GREAT JOB!</h1>
  </div>

</div>
<script>
  document.addEventListener('DOMContentLoaded', function() {
      var textElement = document.querySelector('.myProgress-text');

      // Triggering a reflow to ensure the initial position is off-screen
      textElement.style.opacity = '0';

      setTimeout(function() {
        textElement.style.opacity = '1';
        textElement.style.transform = 'translateX(0)';
      }, 100); // Adjust the delay as needed
    });
</script>
@endsection
