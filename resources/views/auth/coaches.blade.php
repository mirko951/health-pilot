@extends('auth.layouts')

@section('content')
<div class="container-fluid mt-5">
    <div class="row">
        <div class="col-md-12 title-bg">
            <div class="position-absolute top-0 start-0 h-100 w-100 d-flex align-items-center justify-content-start exercise-name-bg">
                <p class="uppercase exercises-recipes-text orange-text">Coaches</p>
            </div>
        </div>
    </div>
</div>
<div class="container">
    <h1 style="color:#ffff">Do you want a personalized plan?</h1>
    <h1 style="color:#ffff">Contact one of our professionists!</h1>
    <br>
    <h2 style="color:#ffff">Our Coaches:</h2>
    <div class="row">
        @foreach ($coaches as $coach)
            <div class="col-md-4 mb-4">
                <div class="card" style="background-color: rgba(237, 139, 24, 0.7);">
                    <img src="{{ $coach->photo }}" alt="{{ $coach->name }}" class="card-img-top">
                    <div class="card-body">
                        <h5 class="card-title" style="color: #ffff;">{{ $coach->name }}</h5>
                        <p class="card-text" style="color: #ffff;">Email: {{ $coach->email_address }}</p>
                        <p class="card-text" style="color: #ffff;">Description: {{ $coach->description }}</p>
                        <a href="{{ route('reviews.create', ['coach_id' => $coach->id]) }}" class="btn btn-primary"  style="color: #ffff;">Leave Review</a>
                        @if ($coach->reviews && !$coach->reviews->isEmpty())
                            @php
                                $averageRating = $coach->reviews->avg('rating');
                            @endphp
                            <p style="color: #ffff;">Average Rating: {{ number_format($averageRating, 2) }}/5</p>
                        @else
                            <p style="color: #ffff;">No reviews available.</p>
                        @endif
                    </div>
                </div>
            </div>
        @endforeach
    </div>
</div>
<script>
    document.addEventListener('DOMContentLoaded', function() {
      var textElement = document.querySelector('.exercises-recipes-text');

      // Triggering a reflow to ensure the initial position is off-screen
      textElement.style.opacity = '0';

      setTimeout(function() {
        textElement.style.opacity = '1';
        textElement.style.transform = 'translateX(0)';
      }, 100); // Adjust the delay as needed
    });
</script>
@endsection
