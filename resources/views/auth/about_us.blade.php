@extends('auth.layouts')
@section('content')
<div class="container-fluid" style="background-image: url('/resources/images/dashboard/man-with-powder.jpg'); background-size: cover; background-attachment: fixed;">
<div class="container mt-5">
    <h1 class="text-center mb-4 orange-text-about-us" style="font-size: 80px; padding-top: 40px !important;">About Us - Health Pilot</h1>

    <!-- Mission Section -->
    <section>
        <h2 class="orange-text-about-us">Our Mission</h2>
        <p class="p-about-us">
            At Health Pilot, our mission is simple yet profound: to make healthy living accessible to everyone, regardless of their age, background, or fitness level.
            We believe that a healthy lifestyle is not a luxury but a fundamental right, and we are committed to providing the tools and resources needed to make it a reality for all.
        </p>
    </section>

    <!-- Wellness Journey Section -->
    <section>
        <h2 class="orange-text-about-us">Your Journey to Wellness</h2>
        <p class="p-about-us">
            Embarking on a journey to a healthier, happier you can be both exciting and challenging. Health Pilot is here to guide you every step of the way.
            We understand that there is no one-size-fits-all solution when it comes to health and fitness. That's why our application is designed to be highly customizable, allowing you to tailor your experience to your unique goals and preferences.
        </p>
    </section>

    <!-- Key Features Section -->
    <section>
        <h2 class="orange-text-about-us">Key Features of Health Pilot</h2>
        <div class="row">
            <div class="col-md-6">
                <h3 class="orange-text-about-us">Exercise Plans</h3>
                <p class="p-about-us">Our application offers a wide range of exercise plans suitable for all fitness levels. Whether you're a beginner or an experienced athlete, you'll find workouts that fit your needs and goals. Our detailed video tutorials ensure you perform exercises correctly and safely.</p>
            </div>
            <div class="col-md-6">
                <h3 class="orange-text-about-us">Meal Recipes</h3>
                <p class="p-about-us">Eating well is a crucial component of a healthy lifestyle. Health Pilot provides a diverse collection of nutritious and delicious meal recipes. From quick and easy meals to gourmet options, we've got you covered. You can even customize meal plans to suit your dietary requirements.</p>
            </div>
            <div class="col-md-6">
                <h3 class="orange-text-about-us">Progress Tracking</h3>
                <p class="p-about-us">We believe that progress is a powerful motivator. With Health Pilot, you can easily track your fitness journey, including weight loss, muscle gain, and other health metrics. Celebrate your achievements and stay motivated to reach your desired weight and overall wellness.</p>
            </div>
            <div class="col-md-6">
                <h3 class="orange-text-about-us">Expert Guidance</h3>
                <p class="p-about-us">Our team of fitness and nutrition experts has crafted the exercise plans and meal recipes, ensuring you receive the best advice and support.</p>
            </div>
        </div>
    </section>

 <!-- Preview of Exercises Accordion -->
<section>
    <h2 class="orange-text-about-us">Preview of Exercises</h2>
    <div class="accordion accordion-flush" id="exerciseAccordion">
        @foreach ($exercises as $exercise)
            <div class="accordion-item orange-bg white-text">
                <h2 class="accordion-header" id="exerciseHeader{{$exercise->exercise_id}}">
                    <button class="accordion-button collapsed orange-bg" type="button" data-bs-toggle="collapse" data-bs-target="#exerciseCollapse{{$exercise->exercise_id}}" aria-expanded="false" aria-controls="exerciseCollapse{{$exercise->exercise_id}}">
                        <img src="{{asset($exercise->image_url)}}" alt="">
                        <p class="uppercase" style="text-align: right;">{{$exercise->name}}</p>
                    </button>
                </h2>
                <div id="exerciseCollapse{{$exercise->exercise_id}}" class="accordion-collapse collapse" aria-labelledby="exerciseHeader{{$exercise->exercise_id}}">
                    <div class="accordion-body black-bg"> 
                        <p>{{$exercise->description}}</p>
                        <p>Repetitions: {{$exercise->rep}}</p>
                        <p>Calories burnt: {{$exercise->calories}}</p>
                    </div>
                </div>
            </div>
        @endforeach
    </div>
</section>

<!-- Preview of Recipes Accordion -->
<section>
    <h2 class="orange-text-about-us">Preview of Recipes</h2>
    <div class="accordion accordion-flush" id="recipeAccordion">
        @foreach ($recipes as $recipe)
            <div class="accordion-item orange-bg white-text">
                <h2 class="accordion-header" id="recipeHeader{{$recipe->recipe_id}}">
                    <button class="accordion-button collapsed orange-bg" type="button" data-bs-toggle="collapse" data-bs-target="#recipeCollapse{{$recipe->recipe_id}}" aria-expanded="false" aria-controls="recipeCollapse{{$recipe->recipe_id}}">
                        <img src="{{asset($recipe->image_url)}}" alt="">
                        <p class="uppercase">{{$recipe->name}}</p>
                    </button>
                </h2>
                <div id="recipeCollapse{{$recipe->recipe_id}}" class="accordion-collapse collapse" aria-labelledby="recipeHeader{{$recipe->recipe_id}}">
                    <div class="accordion-body black-bg"> 
                        <p>{{$recipe->ingredients}}</p>
                        <p>{{$recipe->method}}</p>
                        <p>{{$recipe->calories_per_serving}} kcal</p>
                    </div>
                </div>
            </div>
        @endforeach
    </div>
</section>




    <!-- Why Choose Us Section -->
    <section>
        <h2 class="orange-text-about-us">Why Choose Health Pilot?</h2>
            <p class="p-about-us">Free of Charge: We believe that everyone deserves access to tools for better health, which is why Health Pilot is completely free to use.</p>
            <p class="p-about-us">Easy Registration: Signing up for Health Pilot is a breeze. All you need is an email address, and you'll be on your way to a healthier you in no time.</p>
            <p class="p-about-us">Privacy and Security: Your privacy and data security are paramount. Rest assured that your information is safe with us.</p>
    </section>

    <!-- Join Us Section -->
    <section class="text-center mt-5">
        <h2 class="orange-text-about-us">Join the Health Pilot Community Today!</h2>
        <p class="p-about-us">Are you ready to take the first step towards a healthier, happier you? Join the Health Pilot community today and let us be your co-pilot on this incredible journey.
            Whether your goal is to shed a few pounds, increase your energy levels, or simply feel better in your own skin, we're here to help you succeed.
        </p>
        @guest
        <a href="{{ route('register') }}" class="btn" style="background-color: rgba(237, 139, 24); color: white;">Sign Up Now</a>
        @endguest
    </section>
</div>
</div>
@endsection