@extends('auth.layouts')

@section('content')

    <div class="container-fluid mt-5">
        <div class="row">
            <div class="col-md-12 title-bg">
                <div class="position-absolute top-0 start-0 h-100 w-100 d-flex align-items-center justify-content-start recipe-name-bg">
                    <p class="uppercase exercises-recipes-text orange-text">{{$recipe_category->name}}</p>
                </div>
            </div>
        </div>
    </div>
<div class="container">
    <form action="{{ route('savemeals') }}" method="post">
    @csrf
    <div class="accordion accordion-flush" id="accordionFlushExample">
        @foreach ($recipes as $recipe)
        <div class="accordion-item orange-bg white-text">
            <h2 class="accordion-header" id="flush-{{$recipe->recipe_id}}">
            <button class="accordion-button collapsed orange-bg" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapse{{$recipe->recipe_id}}" aria-expanded="false" aria-controls="flush-collapse{{$recipe->recipe_id}}">
            <img src="{{asset($recipe->image_url)}}" alt="">
            <p class="uppercase">{{$recipe->name}}</p>
            </button>
            </h2>
            <div id="flush-collapse{{$recipe->recipe_id}}" class="accordion-collapse collapse" aria-labelledby="flush-{{$recipe->recipe_id}}">
                <div class="accordion-body black-bg"> 
                    <p>{{$recipe->ingredients}}</p>
                    <p>{{$recipe->method}}</p>
                    <p>{{$recipe->calories_per_serving}} kcal</p>
                    <div class="form-check form-switch">
                        <p><input class="form-check-input" type="checkbox" name="selected_recipes[]" value="{{$recipe->recipe_id}}"> Done</p>   
                    </div>
                </div>
            </div>
        </div>
        @endforeach
    </div>
    <br>
        <div class="d-grid gap-2">
            <button type="submit" class="btn orange-bg white-text btn-lg">Save</button>
        </div>
</form>
</div>

<script>
    document.addEventListener('DOMContentLoaded', function() {
      var textElement = document.querySelector('.exercises-recipes-text');

      // Triggering a reflow to ensure the initial position is off-screen
      textElement.style.opacity = '0';

      setTimeout(function() {
        textElement.style.opacity = '1';
        textElement.style.transform = 'translateX(0)';
      }, 100); // Adjust the delay as needed
    });
</script>
@endsection
