
<!DOCTYPE html>
<html>
<head>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css">
    <script src="https://cdn.jsdelivr.net/npm/jquery@3.6.0/dist/jquery.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js"></script>
    <link href="{{asset('resources/css/app_cart.css')}}" rel="stylesheet">
    <link rel="stylesheet" href="{{asset('resources/css/app.css')}}">
</head>
<body>
    <nav class="navbar navbar-expand-lg navbarclass fixed-top">
        <div class="container-fluid">
            <a class="navbar-brand" href="{{ route('dashboard') }}">HealthPilot</a>
                <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
                    <div class="navbar-nav">
                        <a class="nav-link" href="{{ route('dashboard') }}">Home</a>
                        <a class="nav-link" href="{{ route('about_us') }}">About us</a>
                        @auth
                            <a class="nav-link" href="{{ route('exerciseslist') }}">Exercises</a>
                            <a class="nav-link" href="{{ route('recipeslist') }}">Recipes</a>
                            <a class="nav-link" href="{{ route('coach') }}">Coach</a>
                            <a class="nav-link" href="{{ route('products') }}">Shop</a>
                        @endauth
                
                        @guest
                            <a data-toggle="modal" data-target="#exampleModal" href="#account" style="margin-top: 5px;">Account</a>
                        @endguest

                        @auth
                            <a class="nav-link" href="{{ route('account') }}">Account</a>
                        @endauth
                            <a class="nav-link" href="#"><img src="{{asset('resources/images/icons/icons8-twitter-50.png')}}" alt="Twitter" width="30" height="30"></a>
                            <a class="nav-link" href="#"><img src="{{asset('resources/images/icons/icons8-instagram-50.png')}}" alt="Instagram" width="30" height="30"></a>
                            <a class="nav-link" href="#"> <img src="{{asset('resources/images/icons/icons8-facebook-50.png')}}" alt="Facebook" width="30" height="30">
                            <a class="nav-link" href="#"><img src="{{asset('resources/images/icons/icons8-youtube-50.png')}}" alt="YouTube" width="30" height="30"></a>
                            <a class="nav-link" href="#"><img src="{{asset('resources/images/icons/icons8-tiktok-50.png')}}" alt="TikTok" width="30" height="30"></a>
                        @auth
                            @if(auth()->user()->admin == 1)
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ route('admin.exercises') }}">Admin</a>
                                </li>
                            @endif
                        @endauth  
                </div>
            </div>
        </div>
    </nav>

    <div class="container" style="margin-top: 100px !important;">
        <div class="row">
            <div class="col-lg-12 col-sm-12 col-12">
                <div class="dropdown">
                    
                    <button id="dLabel" type="button" class="btn btn-warning" data-bs-toggle="dropdown">
                        <i class="fa fa-shopping-cart" aria-hidden="true"></i> Cart <span class="badge bg-danger">{{ count((array) session('cart')) }}</span>
                    </button>
    
                    <div class="dropdown-menu" aria-labelledby="dLabel">
                        <div class="row total-header-section">
                            @php $total = 0 @endphp
                            @foreach((array) session('cart') as $id => $details)
                                @php $total += $details['price'] * $details['quantity'] @endphp
                            @endforeach
                            <div class="col-lg-12 col-sm-12 col-12 total-section text-right">
                                <p>Total: <span class="text-success">$ {{ $total }}</span></p>
                            </div>
                        </div>
                        @if(session('cart'))
                            @foreach(session('cart') as $id => $details)
                                <div class="row cart-detail">
                                    <div class="col-lg-4 col-sm-4 col-4 cart-detail-img">
                                        <img src="{{ asset($details['photo']) }}" width="100" height="100" class="img-responsive"/>
                                    </div>
                                    <div class="col-lg-8 col-sm-8 col-8 cart-detail-product">
                                        <p>{{ $details['product_name'] }}</p>
                                        <span class="price text-success"> ${{ $details['price'] }}</span> <span class="count"> Quantity:{{ $details['quantity'] }}</span>
                                    </div>
                                </div>
                            @endforeach
                        @endif
                        <div class="row">
                            <div class="col-lg-12 col-sm-12 col-12 text-center checkout">
                                <a href="{{ route('cart') }}" class="btn btn-warning btn-block">View all</a>
                            </div>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
    <br/>
    <div class="container">
        
        @if(session('success'))
            <div class="alert alert-success">
            {{ session('success') }}
            </div> 
        @endif
        
        @yield('content')
    </div>  
        @yield('scripts')
</body>
</html>