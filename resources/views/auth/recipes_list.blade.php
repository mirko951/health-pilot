@extends('auth.layouts')
@section('content')


    <div class="container-fluid mt-5">
            <div class="row">
                <div class="col-md-12 title-bg">
                <div class="position-absolute top-0 start-0 h-100 w-100 d-flex align-items-center justify-content-start recipes-bg">
                    <p class="exercises-recipes-text orange-text">RECIPES</p>
                </div>
                </div>
            </div>
    </div>
<div class="container">
    <ul>
    @foreach ($recipes_list as $recipe)
    <li>
        <div class="row mt-4">
            <div class="col-md-12">
                <div class="full-width list-bg" style="margin-bottom: 50px;">
                    <img src="{{asset($recipe->images)}}" class="list-image">
                    <a class="title-list" href="{{URL::to('recipes', [$recipe->shortname])}}">
                        <p class="uppercase align-right">{{$recipe->name}}</p>
                    </a>
                </div>
            </div>
        </div>
    </li>
    @endforeach
    </ul>
</div>
<script>
    document.addEventListener('DOMContentLoaded', function() {
      var textElement = document.querySelector('.exercises-recipes-text');

      // Triggering a reflow to ensure the initial position is off-screen
      textElement.style.opacity = '0';

      setTimeout(function() {
        textElement.style.opacity = '1';
        textElement.style.transform = 'translateX(0)';
      }, 100); // Adjust the delay as needed
    });

    document.addEventListener('DOMContentLoaded', function() {
        var listImages = document.querySelectorAll('.list-image');
        var titleList = document.querySelectorAll('.title-list');

        // Triggering a reflow to ensure the initial position is off-screen
        listImages.forEach(function(image) {
            image.style.opacity = '0';
        });

        titleList.forEach(function(title) {
            title.style.opacity = '0';
        });

        setTimeout(function() {
            listImages.forEach(function(image) {
                image.style.opacity = '1';
                image.style.transform = 'translateX(0)';
            });

            titleList.forEach(function(title) {
                title.style.opacity = '1';
                title.style.transform = 'translateX(0)';
            });
        }, 100); // Adjust the delay as needed
    });
  </script>
    @endsection