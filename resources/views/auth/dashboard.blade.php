@extends('auth.layouts')

@section('content')

    <div class="container-fluid">
        <!-- First Section -->
        <div class="row">
            <div class="col-12 mb-4 text-white p-3 mt-5 center-content" id="first-div">
                <h1 class="mt-5 ml15"><span class="word">Health</span><span class="word">Pilot</span></h1>
                @guest
                <a class="btn mt-3" id="join" href="{{ route('login') }}">Join Now</a>
                @else
                <p class="else-wellcome">Wellcome, {{ Auth::user()->name }}!</p>
                @endguest
            </div>
        </div>
    </div>

    <div class="container-fluid">
        <!-- Second Section -->
        <div class="row" id="second-div">
            <div class="col-12 mb-4 text-white p-3 mt-5" >
                <h2 class="intro">
                    <span>Feel</span> <span>better</span> <span>with</span> <span>yourself</span><br>
                    <span>Improve</span> <span>your</span> <span>strength,</span> <span>lose</span> <span>weight,</span> <span>build</span> <span>muscles.</span><br>
                    <span>Eat</span> <span>healthy!</span>
                </h2>
            </div>
        </div>
    </div>

    <div class="container">
        <!-- About Us Section -->
        <div class="row">
            <div class="col-md-6 mb-4 text-white p-3">
                <h2 class="wellcome abbassa">
                    Welcome to Health Pilot
                </h2>
                <h3 class="about">About us</h3>
                <p class="wellcome-about-us">Welcome to Health Pilot,<br>your trusted partner on the journey to a healthier and happier you!<br>We are thrilled to introduce you to our free,<br>user-friendly application designed to empower individuals like you<br>to take control of their health and achieve their desired weight<br>through a combination of effective exercises<br>and delicious meal recipes.</p>
            </div>
            <div class="col-md-6 mb-4 p-3">
                <img src="resources/images/dashboard/workout1.JPEG" class="img-fluid">
            </div>
        </div>

        <!-- Carousel Section -->
        <div class="row">
            <div class="col-md-12 mb-4 p-3">
                <div id="carouselExampleCaptions" class="carousel slide" data-bs-ride="carousel">
                    <div class="carousel-indicators">
                        <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="0" class="active" aria-current="true" aria-label="Slide 1"></button>
                        <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="1" aria-label="Slide 2"></button>
                        <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="2" aria-label="Slide 3"></button>
                        <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="3" aria-label="Slide 4"></button>
                        <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="4" aria-label="Slide 5"></button>
                    </div>
                    <div class="carousel-inner">
                        <div class="carousel-item active">
                            <img src="resources/images/dashboard/workout.jpg" class="d-block carousel-photo-dimension" alt="Slide 1">
                            <div class="carousel-caption d-none d-md-block">
                                <h5>First slide label</h5>
                                <p>Some representative placeholder content for the first slide.</p>
                            </div>
                        </div>
                        <div class="carousel-item">
                            <img src="resources/images/dashboard/treadmill.jpg" class="d-block carousel-photo-dimension" alt="Slide 2">
                            <div class="carousel-caption d-none d-md-block">
                                <h5>Second slide label</h5>
                                <p>Some representative placeholder content for the second slide.</p>
                            </div>
                        </div>
                        <div class="carousel-item">
                            <img src="resources/images/recipes/breakfast.png" class="d-block carousel-photo-dimension" alt="Slide 3">
                            <div class="carousel-caption d-none d-md-block">
                                <h5>Third slide label</h5>
                                <p>Some representative placeholder content for the third slide.</p>
                            </div>
                        </div>
                        <div class="carousel-item">
                            <img src="resources/images/recipes/tofukebap.JPEG" class="d-block carousel-photo-dimension" alt="Slide 4">
                            <div class="carousel-caption d-none d-md-block">
                                <h5>Fourth slide label</h5>
                                <p>Some representative placeholder content for the fourth slide.</p>
                            </div>
                        </div>
                        <div class="carousel-item">
                            <img src="resources/images/recipes/burritobowl.JPEG" class="d-block carousel-photo-dimension" alt="Slide 5">
                            <div class="carousel-caption d-none d-md-block">
                                <h5>Fifth slide label</h5>
                                <p>Some representative placeholder content for the fifth slide.</p>
                            </div>
                        </div>
                    </div>
                    <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide="prev">
                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                        <span class="visually-hidden">Previous</span>
                    </button>
                    <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide="next">
                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                        <span class="visually-hidden">Next</span>
                    </button>
                </div>
            </div>
        </div>
    </div>
        <!-- Enter Our Community Section -->
        <div class="container">
            <div class="row">
                <div class="col-md-6 mb-4 text-white p-3">
                    <p class="description">Enter our community,<br>Discover all the exercises we have for you.<br>Build a better you.</p>
                </div>
                <div class="col-md-6 mb-4 p-3">
                    <img src="resources/images/dashboard/man-with-powder.jpg" class="side-img">
                </div>
            </div>
        </div>
            <!-- Learn to Eat Healthy Section -->
        <div class="container">
            <div class="row">
                <div class="col-md-6 mb-4 p-3">
                    <img src="resources/images/dashboard/strawberries.jpg"  class="side-img">
                </div>
                <div class="col-md-6 mb-4 text-white p-3">
                    <p class="description">Learn how to eat healthy.<br>Here you will find plenty of recipes.<br>What are you waiting for?</p>
                </div>
            </div>
        </div>   
       
        <!-- Start Today Section -->
    <div class="container">
        <div class="row">
            @guest <!-- Verifica se l'utente NON è autenticato -->
                <div class="col-md-6 mb-4 d-none d-lg-block text-white p-3 text-center start-div" >
                    <h1>START TODAY</h1>
                    <a class="btn mt-3" href="{{ route('login') }}" id="join">JOIN NOW</a>
                </div>

                <div class="col-sm-12 mb-4 text-white text-center d-lg-none p-3 center-content start-div" >
                    <h1>START TODAY</h1>
                    <a class="btn mt-3" href="{{ route('login') }}" id="join">JOIN NOW</a>
                </div>
                @else <!-- Se l'utente è autenticato, mostra un altro contenuto al suo posto -->
                <div class="col-md-6 mb-4 d-none d-lg-block text-white p-3 text-center start-div" >
                        <h1>START TODAY</h1>
                        <p style="font-size: 30px;">Reach your goal</p>
                        <p style="font-size: 30px;">Use our <a href="{{ route('exerciseslist') }}">exercises</a> <br>
                           and our <a href="{{ route('recipeslist') }}">recipes</a>.
                        </p>
                        
                    </div>

                <div class="col-sm-12 mb-4 text-white text-center d-lg-none p-3 center-content start-div">
                    <h1>START TODAY</h1>
                        <p style="font-size: 30px;">Reach your goal</p>
                        <p style="font-size: 30px;">Use our <a href="">exercises</a> <br>
                           and our <a href="">recipes</a>.
                        </p>
                </div>    
                @endguest
                <div class="col-md-6 mb-4 text-white p-3">
                <img src="resources/images/dashboard/fit-girl.jpg" class="side-img" alt="">
            </div>
        </div>
    </div>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/animejs/2.0.2/anime.min.js"></script>
    <script>
        anime.timeline()
          .add({
            targets: '.ml15 .word',
            scale: [10, 1],
            opacity: [0, 1],
            easing: "easeOutCirc",
            duration: 500,
            delay: (el, i) => 500 * i
          });
      </script>
    
@endsection