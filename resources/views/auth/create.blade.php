@extends('auth.layouts')

@section('content')
    <h1 style="margin-top: 100px;">Leave a Review</h1>
    
    @if(session('success'))
        <div class="alert alert-success">
            {{ session('success') }}
        </div>
    @endif

    <form method="POST" action="{{ route('reviews.store') }}">
        @csrf
        <input type="hidden" name="coach_id" value="{{$coach_id}}">
        
        <div class="form-group">
            <label for="rating">Rating</label>
            <div class="rating"> 
                <input type="radio" name="rating" value="5" id="5"><label for="5">☆</label> <input type="radio" name="rating" value="4" id="4"><label for="4">☆</label> <input type="radio" name="rating" value="3" id="3"><label for="3">☆</label> <input type="radio" name="rating" value="2" id="2"><label for="2">☆</label> <input type="radio" name="rating" value="1" id="1"><label for="1">☆</label>
            </div>
        </div>


        <div class="form-group">
            <label for="comment">Comment</label>
            <textarea name="comment" class="form-control" required></textarea>
        </div>

        <button type="submit" class="btn btn-primary">Submit Review</button>
    </form>
    


@endsection
