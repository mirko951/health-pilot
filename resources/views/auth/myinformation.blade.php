@extends('auth.layouts')

@section('content')

    <div class="container-fluid mt-5">
        <div class="row">
            <div class="col-md-12 title-bg">
                <!-- Div con immagine di sfondo -->
                <div class="position-absolute top-0 start-0 h-100 w-100 d-flex align-items-center justify-content-start myInfo">
                    <p class="myInfo-text uppercase orange-text">my information</p>
                </div>
            </div>
        </div>
    </div>

    <div class="container mt-5">
        <div class="row">
            <div class="col-md-12">
                <p class="text-uppercase" style="color: white; font-size: 30px;">{{$name}}</p>
                <p style="color: white; font-size: 30px;">{{$age}}</p>
                <p class="text-uppercase" style="color: white; font-size: 30px;">{{$gender}}</p>
                <p class="text-uppercase" style="color: white; font-size: 30px;">Weight</p>
                <input id="weight" type="text" class="form-control">
                <p style="color: white; font-size: 20px;">kg</p>
                <p class="text-uppercase" style="color: white; font-size: 30px;">Height</p>
                <input id="height" type="text" class="form-control">
                <p style="color: white; font-size: 20px;">cm</p>
                <button id="btn" class="btn btn-primary">CALCULATE MY BMI</button>
                <h2 class="text-uppercase" style="color: white; font-size: 50px;">Your BMI is: <div id="result"></div></h2>
            </div>
           
        </div>
        <div class="row mt-4">
            <div class="col-md-12">
                <img src="{{asset('resources/images/bmi.jpg')}}" alt="bmi-graphic" class="img-fluid" style="margin-bottom: 100px;">
            </div>
        </div>
    </div>


    <script>
        window.onload = () => {
            let button = document.querySelector("#btn");

            // Function for calculating BMI
            button.addEventListener("click", calculateBMI);
        };

        function calculateBMI() {
            let height = parseInt(document.querySelector("#height").value);
            let weight = parseInt(document.querySelector("#weight").value);
            let result = document.querySelector("#result");

            if (isNaN(height) || height <= 0)
                result.innerHTML = "Provide a valid Height!";
            else if (isNaN(weight) || weight <= 0)
                result.innerHTML = "Provide a valid Weight!";
            else {
                let bmi = (weight / ((height * height) / 10000)).toFixed(2);

                if (bmi < 18.6)
                    result.innerHTML = `Under Weight: <span>${bmi}</span>`;
                else if (bmi >= 18.6 && bmi < 24.9)
                    result.innerHTML = `Normal: <span>${bmi}</span>`;
                else
                    result.innerHTML = `Over Weight: <span>${bmi}</span>`;
            }
        }


        document.addEventListener('DOMContentLoaded', function() {
      var textElement = document.querySelector('.myInfo-text');

      // Triggering a reflow to ensure the initial position is off-screen
      textElement.style.opacity = '0';

      setTimeout(function() {
        textElement.style.opacity = '1';
        textElement.style.transform = 'translateX(0)';
      }, 100); // Adjust the delay as needed
    });
    </script>
    @endsection
