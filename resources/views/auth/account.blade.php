@extends('auth.layouts')

@section('content')
<div class="container">
    <h1 class="account uppercase"><a href="{{ route('myinformation') }}" class="orange-text">my information</a></h1>
    <h1 class="account uppercase"><a href="{{ route('myprogress') }}" class="orange-text">my progress</a></h1>
    <img src="resources/images/three-apples.jpg" alt="apples" class="side-img">
    <form action="{{ route('logout') }}" method="post">
        @csrf
        <button type="submit" class="btn uppercase orange-text" style="font-size: 40px;">log out</button>
    </form>
</div>
@endsection
