
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Health Pilot</title>
    <link rel="stylesheet" href="{{asset('resources/css/app.css')}}">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-4bw+/aepP/YC94hEpVNVgiZdgIC5+VKNBQNGCHeKRQN+PtmoHDEXuppvnDJzQIu9" crossorigin="anonymous">
</head>
<body>
<!-- NAVBAR -->
<nav class="navbar navbar-expand-lg navbarclass fixed-top">
        <div class="container-fluid">
          <a class="navbar-brand" href="{{ route('dashboard') }}">HealthPilot</a>
          <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
          </button>
          <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
            <div class="navbar-nav">
              <a class="nav-link" href="{{ route('dashboard') }}">Home</a>
              <a class="nav-link" href="{{ route('about_us') }}">About us</a>
              @auth
                <a class="nav-link" href="{{ route('exerciseslist') }}">Exercises</a>
                <a class="nav-link" href="{{ route('recipeslist') }}">Recipes</a>
                <a class="nav-link" href="{{ route('coach') }}">Coach</a>
                <a class="nav-link" href="{{ route('products') }}">Shop</a>
              @endauth
              
              @guest
                <a data-toggle="modal" data-target="#exampleModal" href="#account" style="margin-top: 5px;">Account</a>
              @endguest

              @auth
                <a class="nav-link" href="{{ route('account') }}">Account</a>
              @endauth
                <a class="nav-link" href="#"><img src="{{asset('resources/images/icons/icons8-twitter-50.png')}}" alt="Twitter" width="30" height="30"></a>
                <a class="nav-link" href="#"><img src="{{asset('resources/images/icons/icons8-instagram-50.png')}}" alt="Instagram" width="30" height="30"></a>
                <a class="nav-link" href="#"> <img src="{{asset('resources/images/icons/icons8-facebook-50.png')}}" alt="Facebook" width="30" height="30">
                <a class="nav-link" href="#"><img src="{{asset('resources/images/icons/icons8-youtube-50.png')}}" alt="YouTube" width="30" height="30"></a>
                <a class="nav-link" href="#"><img src="{{asset('resources/images/icons/icons8-tiktok-50.png')}}" alt="TikTok" width="30" height="30"></a>
              @auth
                @if(auth()->user()->admin == 1)
                  <li class="nav-item">
                      <a class="nav-link" href="{{ route('admin.exercises') }}">Admin</a>
                  </li>
                @endif
              @endauth  
            </div>
          </div>
        </div>
      </nav>
      </nav>


        @yield('content')

   
        <footer class="bg-dark py-3 mt-4 text-center ">
            <div class="container">
                <p>Health Pilot <img src="{{asset('resources/images/icons/icons8-copyright-50.png')}}" alt="copyright" height="20px">2023 -- 1234, Vienna +43 123456789 email: HealthPilot@domain.at
                </p>
            </div>
        </footer>


<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content orange-bg">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel"><b>LOG IN</b></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body black-bg">
      
      <div class="row justify-content-center mt-5" > 
    <div class="col-md-8" >

        <div class="card black-bg">
            <div class="card-header"></div>
            <div class="card-body">
                <form action="{{ route('authenticate') }}" method="post">
                    @csrf
                    <div class="mb-3 row">
                        <label for="email" class="col-md-4 col-form-label text-md-end text-start white-text">Email</label>
                        <div class="col-md-6">
                          <input type="email" class="form-control @error('email') is-invalid @enderror" id="email" name="email" value="{{ old('email') }}">
                            @if ($errors->has('email'))
                                <span class="text-danger">{{ $errors->first('email') }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="mb-3 row">
                        <label for="password" class="col-md-4 col-form-label text-md-end text-start white-text">Password</label>
                        <div class="col-md-6">
                          <input type="password" class="form-control @error('password') is-invalid @enderror" id="password" name="password">
                            @if ($errors->has('password'))
                                <span class="text-danger">{{ $errors->first('password') }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="mb-3 row">
                        <input type="submit" class="col-md-3 offset-md-5 btn orange-bg white-text" value="Login">
                    </div>
                    
                </form>
                <a class="btn orange-bg white-text" href="{{URL::to('register')}}">Register</a>
            </div>
        </div>
    </div>    
</div>

      </div>
    </div>
  </div>
</div>

<script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.1/dist/umd/popper.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-HwwvtgBNo3bZJJLYd8oVXjrBZt8cqVSpeBNS5n7C8IVInixGAoxmnlMuBnhbgrkm" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/js/bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css"></script>
</body>
</html>